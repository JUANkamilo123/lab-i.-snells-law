var output2 = document.getElementById("output2");
var datos = document.getElementById("datos");
var datos2 = document.getElementById("datos2");
var deltax2 = document.getElementById("deltax2");
var deltay2 = document.getElementById("deltay2");
var c2 = document.getElementById("c2");
var ctx2 = c2.getContext("2d");

var cw = (c2.width = 300),
  cx = cw / 2;
var ch = (c2.height = 300),
  cy = ch / 2;
//var rad = Math.PI / 180;
var R = 100,
  r = 5;

var handle2 = {
  x: cx + R,
  y: cy,
  r: 5,
};
output2.style.top = handle2.y - 30 + "px";
output2.style.left = handle2.x - 30 + "px";

var isDragging = false;
ctx2.strokeStyle = "#555";
ctx2.fillStyle = "#e18728";
// circle

drawAxes2();
strokeCircle2(cx, cy, R);
strokeCircle3(cx, cy, R);
//drawHandle2(handle2);
drawHub2();

// Events ***************************

c2.addEventListener(
  "mousedown",
  function (evt) {
    isDragging = true;
    updateHandle2(evt);
  },
  false
);

// mousemove
c2.addEventListener(
  "mousemove",
  function (evt) {
    if (isDragging) {
      updateHandle2(evt);
    }
  },
  false
);
// mouseup
c2.addEventListener(
  "mouseup",
  function () {
    isDragging = false;
  },
  false
);
// mouseout
c2.addEventListener(
  "mouseout",
  function (evt) {
    isDragging = false;
  },
  false
); /**/

// Helpers ***************************
function strokeCircle2(x, y, r) {
  ctx2.beginPath();
  ctx2.arc(x, y, r, 0, 2 * Math.PI);
  ctx2.lineWidth = 3;
  ctx2.strokeStyle = "black";
  ctx2.stroke();

  var elCanvas2 = document.getElementById("c2");
  if (elCanvas2 && elCanvas2.getContext) {
    var context2 = elCanvas2.getContext("2d");
    context2.beginPath();
    if (context2) {
      var aPartida2 = (Math.PI / 180) * 270;
      var aFinal2 = (Math.PI / 180) * 320;
      context2.strokeStyle = "blue";
      context2.lineWidth = 3;
      context2.arc(x, y, r, aPartida2, aFinal2, false);
      context2.stroke();
    }
  }
}

function strokeCircle3(x, y, r) {
  ctx2.beginPath();
  ctx2.arc(x, y, r, 0, 2 * Math.PI);
  ctx2.lineWidth = 3;
  ctx2.strokeStyle = "black";
  ctx2.stroke();

  var elCanvas2 = document.getElementById("c2");
  if (elCanvas2 && elCanvas2.getContext) {
    var context2 = elCanvas2.getContext("2d");
    context2.beginPath();
    if (context2) {
      var aPartida2 = (Math.PI / 180) * 270;
      var aFinal2 = (Math.PI / 180) * 320;
      context2.strokeStyle = "blue";
      context2.lineWidth = 3;
      context2.arc(x, y, r, aPartida2, aFinal2, false);
      context2.stroke();
    }
  }
}

function fillCircle2(x, y, r) {
  ctx2.beginPath();
  ctx2.arc(x, y, r, 0, 2 * Math.PI);
  ctx2.save();
  ctx2.strokeStyle = "#ccc";
  ctx2.lineWidth = 3;
  ctx2.fill();
  ctx2.stroke();
  ctx2.restore();
}

function drawHub2() {
  ctx2.save();
  ctx2.fillStyle = "black";
  fillCircle2(cx, cy, 3);
  ctx2.restore();
}

function drawAxes2() {
  ctx2.save();
  ctx2.setLineDash([4, 4]);
  ctx2.strokeStyle = "#ccc";
  ctx2.beginPath();
  ctx2.moveTo(cx, 0);
  ctx2.lineTo(cx, ch); //Recta eje vertical
  ctx2.moveTo(0, cy); // Recta eje horizontal
  ctx2.lineTo(cw, cy);
  ctx2.stroke();
  ctx2.restore();
}

function oMousePos2(canvas, evt) {
  var rect = canvas.getBoundingClientRect();
  return {
    x: Math.round(evt.clientX - rect.left),
    y: Math.round(evt.clientY - rect.top),
  };
}

// function drawHandle2(handle2, x, y, r) {
//   // Linea de eje

//   ctx2.beginPath();
//   ctx2.moveTo(cx, cy);
//   ctx2.lineTo(handle2.x, handle2.y);
//   ctx2.stroke();

//   fillCircle2(handle2.x, handle2.y, handle2.r);
// }

// Obtención del coeficiente de Refracción
function ShowSelected(x, y, r) {}

function updateHandle2(evt) {
  var m = oMousePos2(c2, evt);
  var deltaX = m.x - cx;
  var deltaY = m.y - cy;
  handle2.a = Math.atan2(deltaY, deltaX);
  handle2.b = Math.atan(deltaY / deltaX);
  handle2.x = cx + R * Math.cos(handle2.a);
  handle2.y = cy + R * Math.sin(handle2.a);
  ctx2.clearRect(0, 0, cw, ch);

  drawAxes2();
  strokeCircle2(cx, cy, R);
  strokeCircle3(cx, cy, R);
  //drawHandle2(handle);
  drawHub2();

  output2.innerHTML = atan2.innerHTML + "°";
  datos.innerHTML = atan2.innerHTML;

  var thehta_refracción_n2 = document.getElementById("atan2").innerHTML;
  console.log(thehta_refracción_n2);

  /* Para obtener el valor */
  var nombre1 = document.getElementById("coeficiente n2").value;
  console.log(nombre1);
  var nombre2 = document.getElementById("coeficiente n3").value;
  console.log(nombre2);

  /* Para obtener el texto */
  var combo1 = document.getElementById("coeficiente n2");
  var selected1 = combo1.options[combo1.selectedIndex].text;
  var combo2 = document.getElementById("coeficiente n3");
  var selected2 = combo2.options[combo2.selectedIndex].text;

  console.log(selected1);
  console.log(selected2);
  n2 = parseFloat(selected1, 10);
  n3 = parseFloat(selected2, 10);

  let gra_rad = thehta_refracción_n2 * (Math.PI / 180);
  let n = (n2 / n3) * Math.sin(gra_rad);
  var ns = Math.asin(n);
  let thehta_refracción_large = ns * (180 / Math.PI);
  console.log(gra_rad);
  console.log(n);
  console.log(ns);
  console.log(thehta_refracción_large);

  //FIN de la distancia reccorida. Continuacion con el ángulo de refracción
  let multiplier = 100;
  let thehta_refracción =
    Math.round(thehta_refracción_large * multiplier) / multiplier;
  datos2.innerHTML = thehta_refracción;

  var thehta_critico = Math.asin(n3 / n2);
  var thehta_critico_degree_large = thehta_critico * (180 / Math.PI);
  console.log(thehta_critico);
  console.log(thehta_critico_degree_large); //se imprime angulo critico valor completo
  let thehta_critico_degree =
    Math.round(thehta_critico_degree_large * multiplier) / multiplier; // se redondea el angulo critico
  deltax2.innerHTML = thehta_critico_degree;
  console.log(deltax2);

  //Distancia recorrida en cm

  /* Para obtener el texto */
  var combo = document.getElementById("e2");
  var selected = combo.options[combo.selectedIndex].text;

  console.log(selected);
  e2 = parseFloat(selected, 10);

  var cos_refracción_rad = Math.cos(Math.asin(n));
  var distancia_rad = e2 / cos_refracción_rad;
  console.log(distancia_rad);
  let distancia = Math.round(distancia_rad * multiplier) / multiplier;
  deltay2.innerHTML = distancia;
}

function strokeCircle2(x, y, r) {
  ctx2.beginPath();
  ctx2.arc(x, y, r, 0, 2 * Math.PI);
  ctx2.lineWidth = 3;
  ctx2.strokeStyle = "pink";
  ctx2.stroke();

  let thehta_inci2 = Atan2.innerHTML;
  let thehta_inci2_n = parseFloat(thehta_inci2, 10);
  console.log(thehta_inci2_n);

  var elCanvas = document.getElementById("c2");
  if (elCanvas && elCanvas.getContext) {
    var context = elCanvas.getContext("2d");
    context.beginPath();
    if (context) {
      var aPartida = (Math.PI / 180) * -90;
      var aFinal = (Math.PI / 180) * (-90 + thehta_inci2_n);

      //context.arc(x, y, r, aPartida, aFinal, false);

      var Xfinal = x + r * Math.cos(aPartida);
      var Yfinal = y + r * Math.sin(aPartida);

      ctx2.fillStyle = "#abcdef";
      ctx2.strokeStyle = "#1E90FF";
      ctx2.lineWidth = 3;

      //Dibujo:
      context.beginPath();
      context.moveTo(x, y);
      context.lineTo(Xfinal, Yfinal);
      context.arc(x, y, r, aPartida, aFinal);
      context.closePath();
      context.fill();
      context.stroke();
      console.log(thehta_inci2);
      console.log(aFinal);

      var incidente = parseFloat(Atan2.innerHTML, 10);
      var critico = parseFloat(deltax2.innerHTML, 10);
      console.log(incidente);
      console.log(critico);

      //DATOS TABLA DINAMINCA
      let nombre_materila2 = document.getElementById("coeficiente n2").value;
      material2.innerHTML = nombre_materila2;

      let valor_material2 = document.getElementById("coeficiente n2");
      let indice2 = valor_material2.options[valor_material2.selectedIndex].text;
      valor2.innerHTML = indice2;

      ángulo_incidente2.innerHTML = atan2.innerHTML;
      ángulo_critico2.innerHTML = deltax2.innerHTML;
      ángulo_refracción2.innerHTML = datos2.innerHTML;
      distancia2.innerHTML = deltay2.innerHTML;

      let dis1 = parseFloat(deltay.innerHTML);
      let dis2 = parseFloat(deltay2.innerHTML);
      let dis3 = parseFloat(deltay3.innerHTML);
      let dis4 = parseFloat(deltay4.innerHTML);
      let dis5 = parseFloat(deltay5.innerHTML);
      let dis6 = parseFloat(deltay6.innerHTML);
      let dis7 = parseFloat(deltay7.innerHTML);
      let dis8 = parseFloat(deltay8.innerHTML);
      let dis9 = parseFloat(deltay9.innerHTML);
      let dis10 = parseFloat(deltay10.innerHTML);

      let multiplier = 100;
      distancia_total.innerHTML =
        Math.round(
          (dis1 +
            dis2 +
            dis3 +
            dis4 +
            dis5 +
            dis6 +
            dis7 +
            dis8 +
            dis9 +
            dis10) *
            multiplier
        ) / multiplier;
      distancia_acumuluda2.innerHTML =
        Math.round((dis1 + dis2) * multiplier) / multiplier;
      //FIN TABLA DENAMICA

      if (incidente > critico) {
        // grafica del ángulo critico
        ctx2.beginPath();
        ctx2.arc(x, y, r, 0, 2 * Math.PI);
        ctx2.lineWidth = 3;
        ctx2.strokeStyle = "pink";
        ctx2.stroke();

        let thehta_inci = Atan2.innerHTML;
        let thehta_inci_n = parseFloat(thehta_inci, 10);
        console.log(thehta_inci_n);
        if (elCanvas && elCanvas.getContext) {
          context.beginPath();
          if (context) {
            var aPartida = (Math.PI / 180) * 180;
            var aFinal = (Math.PI / 180) * (-90 - thehta_inci_n);

            //context.arc(x, y, r, aPartida, aFinal, false);

            var Xfinal = x + r * Math.cos(aPartida);
            var Yfinal = y + r * Math.sin(aPartida);

            ctx2.fillStyle = "#abcdef";
            ctx2.strokeStyle = "red";
            ctx2.lineWidth = 3;

            //Dibujo:
            context.beginPath();
            context.moveTo(x, y);
            context.lineTo(Xfinal, Yfinal);
            context.arc(x, y, r, aPartida, aFinal);
            context.closePath();
            context.fill();
            context.stroke();
          }
        }
      }
    }
  }
}

//*********************************************************// */
function strokeCircle3(x, y, r) {
  ctx2.beginPath();
  ctx2.arc(x, y, r, 0, 2 * Math.PI);
  ctx2.lineWidth = 3;
  ctx2.strokeStyle = "pink";
  ctx2.stroke();

  let thehta_refra2 = datos2.innerHTML;
  let thehta_refra2_n = parseFloat(thehta_refra2, 10);
  console.log(thehta_refra2_n);

  var elCanvas = document.getElementById("c2");
  if (elCanvas && elCanvas.getContext) {
    var context = elCanvas.getContext("2d");
    context.beginPath();
    if (context) {
      var aPartida = (Math.PI / 180) * 90;
      var aFinal = (Math.PI / 180) * (90 + thehta_refra2_n);

      //context.arc(x, y, r, aPartida, aFinal, false);

      var Xfinal = x + r * Math.cos(aPartida);
      var Yfinal = y + r * Math.sin(aPartida);

      ctx2.fillStyle = "#abcdef";
      ctx2.strokeStyle = "#1E90FF";
      ctx2.lineWidth = 3;

      //Dibujo:
      context.beginPath();
      context.moveTo(x, y);
      context.lineTo(Xfinal, Yfinal);
      context.arc(x, y, r, aPartida, aFinal);
      context.closePath();
      context.fill();
      context.stroke();
      console.log(thehta_refra2);
      console.log(aFinal);
    }
  }
}
