var output8 = document.getElementById("output8");
var octavo = document.getElementById("octavo");
var octavo2 = document.getElementById("octavo2");
var deltax8 = document.getElementById("deltax8");
var deltay8 = document.getElementById("deltay8");
var c8 = document.getElementById("c8");
var ctx8 = c8.getContext("2d");

var cw = (c8.width = 300),
  cx = cw / 2;
var ch = (c8.height = 300),
  cy = ch / 2;
//var rad = Math.PI / 180;
var R = 100,
  r = 5;

var handle8 = {
  x: cx + R,
  y: cy,
  r: 5,
};
output8.style.top = handle8.y - 30 + "px";
output8.style.left = handle8.x - 30 + "px";

var isDragging = false;
ctx8.strokeStyle = "#555";
ctx8.fillStyle = "#e18728";
// circle

drawAxes8();
strokeCircle14(cx, cy, R);
strokeCircle15(cx, cy, R);
//drawHandle2(handle2);
drawHub8();

// Events ***************************

c8.addEventListener(
  "mousedown",
  function (evt) {
    isDragging = true;
    updateHandle8(evt);
  },
  false
);

// mousemove
c8.addEventListener(
  "mousemove",
  function (evt) {
    if (isDragging) {
      updateHandle8(evt);
    }
  },
  false
);
// mouseup
c8.addEventListener(
  "mouseup",
  function () {
    isDragging = false;
  },
  false
);
// mouseout
c8.addEventListener(
  "mouseout",
  function (evt) {
    isDragging = false;
  },
  false
); /**/

// Helpers ***************************
function strokeCircle14(x, y, r) {
  ctx8.beginPath();
  ctx8.arc(x, y, r, 0, 2 * Math.PI);
  ctx8.lineWidth = 3;
  ctx8.strokeStyle = "black";
  ctx8.stroke();

  var elCanvas8 = document.getElementById("c8");
  if (elCanvas8 && elCanvas8.getContext) {
    var context8 = elCanvas8.getContext("2d");
    context8.beginPath();
    if (context8) {
      var aPartida8 = (Math.PI / 180) * 270;
      var aFinal8 = (Math.PI / 180) * 320;
      context8.strokeStyle = "blue";
      context8.lineWidth = 3;
      context8.arc(x, y, r, aPartida8, aFinal8, false);
      context8.stroke();
    }
  }
}

function strokeCircle15(x, y, r) {
  ctx8.beginPath();
  ctx8.arc(x, y, r, 0, 2 * Math.PI);
  ctx8.lineWidth = 3;
  ctx8.strokeStyle = "black";
  ctx8.stroke();

  var elCanvas8 = document.getElementById("c8");
  if (elCanvas8 && elCanvas8.getContext) {
    var context8 = elCanvas8.getContext("2d");
    context8.beginPath();
    if (context8) {
      var aPartida8 = (Math.PI / 180) * 270;
      var aFinal8 = (Math.PI / 180) * 320;
      context8.strokeStyle = "blue";
      context8.lineWidth = 3;
      context8.arc(x, y, r, aPartida8, aFinal8, false);
      context8.stroke();
    }
  }
}

function fillCircle8(x, y, r) {
  ctx8.beginPath();
  ctx8.arc(x, y, r, 0, 2 * Math.PI);
  ctx8.save();
  ctx8.strokeStyle = "#ccc";
  ctx8.lineWidth = 3;
  ctx8.fill();
  ctx8.stroke();
  ctx8.restore();
}

function drawHub8() {
  ctx8.save();
  ctx8.fillStyle = "black";
  fillCircle8(cx, cy, 3);
  ctx8.restore();
}

function drawAxes8() {
  ctx8.save();
  ctx8.setLineDash([4, 4]);
  ctx8.strokeStyle = "#ccc";
  ctx8.beginPath();
  ctx8.moveTo(cx, 0);
  ctx8.lineTo(cx, ch); //Recta eje vertical
  ctx8.moveTo(0, cy); // Recta eje horizontal
  ctx8.lineTo(cw, cy);
  ctx8.stroke();
  ctx8.restore();
}

function oMousePos8(canvas, evt) {
  var rect = canvas.getBoundingClientRect();
  return {
    x: Math.round(evt.clientX - rect.left),
    y: Math.round(evt.clientY - rect.top),
  };
}

function updateHandle8(evt) {
  var m = oMousePos8(c8, evt);
  var deltaX = m.x - cx;
  var deltaY = m.y - cy;
  handle8.a = Math.atan2(deltaY, deltaX);
  handle8.b = Math.atan(deltaY / deltaX);
  handle8.x = cx + R * Math.cos(handle8.a);
  handle8.y = cy + R * Math.sin(handle8.a);
  ctx8.clearRect(0, 0, cw, ch);

  drawAxes8();
  strokeCircle14(cx, cy, R);
  strokeCircle15(cx, cy, R);
  //drawHandle2(handle);
  drawHub8();

  output8.innerHTML = septimo2.innerHTML + "°";
  octavo.innerHTML = septimo2.innerHTML;

  var thehta_refracción_n2 = document.getElementById("septimo2").innerHTML;
  console.log(thehta_refracción_n2);

  /* Para obtener el valor */
  var nombre1 = document.getElementById("coeficiente n8").value;
  console.log(nombre1);
  var nombre2 = document.getElementById("coeficiente n9").value;
  console.log(nombre2);

  /* Para obtener el texto */
  var combo1 = document.getElementById("coeficiente n8");
  var selected1 = combo1.options[combo1.selectedIndex].text;
  var combo2 = document.getElementById("coeficiente n9");
  var selected2 = combo2.options[combo2.selectedIndex].text;

  console.log(selected1);
  console.log(selected2);
  n8 = parseFloat(selected1, 10);
  n9 = parseFloat(selected2, 10);

  let gra_rad = thehta_refracción_n2 * (Math.PI / 180);
  let n = (n8 / n9) * Math.sin(gra_rad);
  var ns = Math.asin(n);
  let thehta_refracción_large = ns * (180 / Math.PI);
  console.log(gra_rad);
  console.log(n);
  console.log(ns);
  console.log(thehta_refracción_large);

  //FIN de la distancia reccorida. Continuacion con el ángulo de refracción
  let multiplier = 100;
  let thehta_refracción =
    Math.round(thehta_refracción_large * multiplier) / multiplier;
  octavo2.innerHTML = thehta_refracción;

  var thehta_critico = Math.asin(n9 / n8);
  var thehta_critico_degree_large = thehta_critico * (180 / Math.PI);
  console.log(thehta_critico);
  console.log(thehta_critico_degree_large); //se imprime angulo critico valor completo
  let thehta_critico_degree =
    Math.round(thehta_critico_degree_large * multiplier) / multiplier; // se redondea el angulo critico
  deltax8.innerHTML = thehta_critico_degree;
  console.log(deltax8);

  //Distancia recorrida en cm

  /* Para obtener el texto */
  var combo = document.getElementById("e8");
  var selected = combo.options[combo.selectedIndex].text;

  console.log(selected);
  e8 = parseFloat(selected, 10);

  var cos_refracción_rad = Math.cos(Math.asin(n));
  var distancia_rad = e8 / cos_refracción_rad;
  console.log(distancia_rad);
  let distancia = Math.round(distancia_rad * multiplier) / multiplier;
  deltay8.innerHTML = distancia;
}

function strokeCircle14(x, y, r) {
  ctx8.beginPath();
  ctx8.arc(x, y, r, 0, 2 * Math.PI);
  ctx8.lineWidth = 3;
  ctx8.strokeStyle = "pink";
  ctx8.stroke();

  let thehta_inci2 = septimo2.innerHTML;
  let thehta_inci2_n = parseFloat(thehta_inci2, 10);
  console.log(thehta_inci2_n);

  var elCanvas = document.getElementById("c8");
  if (elCanvas && elCanvas.getContext) {
    var context = elCanvas.getContext("2d");
    context.beginPath();
    if (context) {
      var aPartida = (Math.PI / 180) * -90;
      var aFinal = (Math.PI / 180) * (-90 + thehta_inci2_n);

      //context.arc(x, y, r, aPartida, aFinal, false);

      var Xfinal = x + r * Math.cos(aPartida);
      var Yfinal = y + r * Math.sin(aPartida);

      ctx8.fillStyle = "#abcdef";
      ctx8.strokeStyle = "#1E90FF";
      ctx8.lineWidth = 3;

      //Dibujo:
      context.beginPath();
      context.moveTo(x, y);
      context.lineTo(Xfinal, Yfinal);
      context.arc(x, y, r, aPartida, aFinal);
      context.closePath();
      context.fill();
      context.stroke();
      console.log(thehta_inci2);
      console.log(aFinal);

      var incidente = parseFloat(septimo2.innerHTML, 10);
      var critico = parseFloat(deltax8.innerHTML, 10);
      console.log(incidente);
      console.log(critico);

      //DATOS TABLA DINAMINCA
      let nombre_materila2 = document.getElementById("coeficiente n8").value;
      material8.innerHTML = nombre_materila2;

      let valor_material2 = document.getElementById("coeficiente n8");
      let indice2 = valor_material2.options[valor_material2.selectedIndex].text;
      valor8.innerHTML = indice2;

      ángulo_incidente8.innerHTML = septimo2.innerHTML;
      ángulo_critico8.innerHTML = deltax8.innerHTML;
      ángulo_refracción8.innerHTML = octavo2.innerHTML;
      distancia8.innerHTML = deltay8.innerHTML;

      //suma de las distancias
      let dis1 = parseFloat(deltay.innerHTML);
      let dis2 = parseFloat(deltay2.innerHTML);
      let dis3 = parseFloat(deltay3.innerHTML);
      let dis4 = parseFloat(deltay4.innerHTML);
      let dis5 = parseFloat(deltay5.innerHTML);
      let dis6 = parseFloat(deltay6.innerHTML);
      let dis7 = parseFloat(deltay7.innerHTML);
      let dis8 = parseFloat(deltay8.innerHTML);
      let dis9 = parseFloat(deltay9.innerHTML);
      let dis10 = parseFloat(deltay10.innerHTML);

      let multiplier = 100;
      distancia_total.innerHTML =
        Math.round(
          (dis1 +
            dis2 +
            dis3 +
            dis4 +
            dis5 +
            dis6 +
            dis7 +
            dis8 +
            dis9 +
            dis10) *
            multiplier
        ) / multiplier;

      distancia_acumuluda8.innerHTML =
        Math.round(
          (dis1 + dis2 + dis3 + dis4 + dis5 + dis6 + dis7 + dis8) * multiplier
        ) / multiplier;
      //FIN TABLA DENAMICA

      if (incidente > critico) {
        // grafica del ángulo critico
        ctx8.beginPath();
        ctx8.arc(x, y, r, 0, 2 * Math.PI);
        ctx8.lineWidth = 3;
        ctx8.strokeStyle = "pink";
        ctx8.stroke();

        let thehta_inci = septimo2.innerHTML;
        let thehta_inci_n = parseFloat(thehta_inci, 10);
        console.log(thehta_inci_n);
        if (elCanvas && elCanvas.getContext) {
          context.beginPath();
          if (context) {
            var aPartida = (Math.PI / 180) * 180;
            var aFinal = (Math.PI / 180) * (-90 - thehta_inci_n);

            //context.arc(x, y, r, aPartida, aFinal, false);

            var Xfinal = x + r * Math.cos(aPartida);
            var Yfinal = y + r * Math.sin(aPartida);

            ctx8.fillStyle = "#abcdef";
            ctx8.strokeStyle = "red";
            ctx8.lineWidth = 3;

            //Dibujo:
            context.beginPath();
            context.moveTo(x, y);
            context.lineTo(Xfinal, Yfinal);
            context.arc(x, y, r, aPartida, aFinal);
            context.closePath();
            context.fill();
            context.stroke();
          }
        }
      }
    }
  }
}

//*********************************************************// */
function strokeCircle15(x, y, r) {
  ctx8.beginPath();
  ctx8.arc(x, y, r, 0, 2 * Math.PI);
  ctx8.lineWidth = 3;
  ctx8.strokeStyle = "pink";
  ctx8.stroke();

  let thehta_refra2 = octavo2.innerHTML;
  let thehta_refra2_n = parseFloat(thehta_refra2, 10);
  console.log(thehta_refra2_n);

  var elCanvas = document.getElementById("c8");
  if (elCanvas && elCanvas.getContext) {
    var context = elCanvas.getContext("2d");
    context.beginPath();
    if (context) {
      var aPartida = (Math.PI / 180) * 90;
      var aFinal = (Math.PI / 180) * (90 + thehta_refra2_n);

      //context.arc(x, y, r, aPartida, aFinal, false);

      var Xfinal = x + r * Math.cos(aPartida);
      var Yfinal = y + r * Math.sin(aPartida);

      ctx8.fillStyle = "#abcdef";
      ctx8.strokeStyle = "#1E90FF";
      ctx8.lineWidth = 3;

      //Dibujo:
      context.beginPath();
      context.moveTo(x, y);
      context.lineTo(Xfinal, Yfinal);
      context.arc(x, y, r, aPartida, aFinal);
      context.closePath();
      context.fill();
      context.stroke();
      console.log(thehta_refra2);
      console.log(aFinal);
    }
  }
}
