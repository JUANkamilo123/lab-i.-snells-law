var output10 = document.getElementById("output10");
var decimo = document.getElementById("decimo");
var decimo2 = document.getElementById("decimo2");
var deltax10 = document.getElementById("deltax10");
var deltay10 = document.getElementById("deltay10");
var c10 = document.getElementById("c10");
var ctx10 = c10.getContext("2d");

var cw = (c10.width = 300),
  cx = cw / 2;
var ch = (c10.height = 300),
  cy = ch / 2;
//var rad = Math.PI / 180;
var R = 100,
  r = 5;

var handle10 = {
  x: cx + R,
  y: cy,
  r: 5,
};
output10.style.top = handle10.y - 30 + "px";
output10.style.left = handle10.x - 30 + "px";

var isDragging = false;
ctx10.strokeStyle = "#555";
ctx10.fillStyle = "#e18728";
// circle

drawAxes10();
strokeCircle18(cx, cy, R);
strokeCircle19(cx, cy, R);
//drawHandle2(handle2);
drawHub10();

// Events ***************************

c10.addEventListener(
  "mousedown",
  function (evt) {
    isDragging = true;
    updateHandle10(evt);
  },
  false
);

// mousemove
c10.addEventListener(
  "mousemove",
  function (evt) {
    if (isDragging) {
      updateHandle10(evt);
    }
  },
  false
);
// mouseup
c10.addEventListener(
  "mouseup",
  function () {
    isDragging = false;
  },
  false
);
// mouseout
c10.addEventListener(
  "mouseout",
  function (evt) {
    isDragging = false;
  },
  false
); /**/

// Helpers ***************************
function strokeCircle18(x, y, r) {
  ctx10.beginPath();
  ctx10.arc(x, y, r, 0, 2 * Math.PI);
  ctx10.lineWidth = 3;
  ctx10.strokeStyle = "black";
  ctx10.stroke();

  var elCanvas10 = document.getElementById("c10");
  if (elCanvas10 && elCanvas10.getContext) {
    var context10 = elCanvas10.getContext("2d");
    context10.beginPath();
    if (context10) {
      var aPartida10 = (Math.PI / 180) * 270;
      var aFinal10 = (Math.PI / 180) * 320;
      context10.strokeStyle = "blue";
      context10.lineWidth = 3;
      context10.arc(x, y, r, aPartida10, aFinal10, false);
      context10.stroke();
    }
  }
}

function strokeCircle19(x, y, r) {
  ctx10.beginPath();
  ctx10.arc(x, y, r, 0, 2 * Math.PI);
  ctx10.lineWidth = 3;
  ctx10.strokeStyle = "black";
  ctx10.stroke();

  var elCanvas10 = document.getElementById("c10");
  if (elCanvas10 && elCanvas10.getContext) {
    var context10 = elCanvas10.getContext("2d");
    context10.beginPath();
    if (context10) {
      var aPartida10 = (Math.PI / 180) * 270;
      var aFinal10 = (Math.PI / 180) * 320;
      context10.strokeStyle = "blue";
      context10.lineWidth = 3;
      context10.arc(x, y, r, aPartida10, aFinal10, false);
      context10.stroke();
    }
  }
}

function fillCircle10(x, y, r) {
  ctx10.beginPath();
  ctx10.arc(x, y, r, 0, 2 * Math.PI);
  ctx10.save();
  ctx10.strokeStyle = "#ccc";
  ctx10.lineWidth = 3;
  ctx10.fill();
  ctx10.stroke();
  ctx10.restore();
}

function drawHub10() {
  ctx10.save();
  ctx10.fillStyle = "black";
  fillCircle10(cx, cy, 3);
  ctx10.restore();
}

function drawAxes10() {
  ctx10.save();
  ctx10.setLineDash([4, 4]);
  ctx10.strokeStyle = "#ccc";
  ctx10.beginPath();
  ctx10.moveTo(cx, 0);
  ctx10.lineTo(cx, ch); //Recta eje vertical
  ctx10.moveTo(0, cy); // Recta eje horizontal
  ctx10.lineTo(cw, cy);
  ctx10.stroke();
  ctx10.restore();
}

function oMousePos10(canvas, evt) {
  var rect = canvas.getBoundingClientRect();
  return {
    x: Math.round(evt.clientX - rect.left),
    y: Math.round(evt.clientY - rect.top),
  };
}

function updateHandle10(evt) {
  var m = oMousePos9(c9, evt);
  var deltaX = m.x - cx;
  var deltaY = m.y - cy;
  handle10.a = Math.atan2(deltaY, deltaX);
  handle10.b = Math.atan(deltaY / deltaX);
  handle10.x = cx + R * Math.cos(handle10.a);
  handle10.y = cy + R * Math.sin(handle10.a);
  ctx10.clearRect(0, 0, cw, ch);

  drawAxes10();
  strokeCircle18(cx, cy, R);
  strokeCircle19(cx, cy, R);
  //drawHandle2(handle);
  drawHub10();

  output10.innerHTML = noveno2.innerHTML + "°";
  decimo.innerHTML = noveno2.innerHTML;

  var thehta_refracción_n2 = document.getElementById("noveno2").innerHTML;
  console.log(thehta_refracción_n2);

  /* Para obtener el valor */
  var nombre1 = document.getElementById("coeficiente n10").value;
  console.log(nombre1);
  var nombre2 = document.getElementById("coeficiente n11").value;
  console.log(nombre2);

  /* Para obtener el texto */
  var combo1 = document.getElementById("coeficiente n10");
  var selected1 = combo1.options[combo1.selectedIndex].text;
  var combo2 = document.getElementById("coeficiente n11");
  var selected2 = combo2.options[combo2.selectedIndex].text;

  console.log(selected1);
  console.log(selected2);
  n10 = parseFloat(selected1, 10);
  n11 = parseFloat(selected2, 10);

  let gra_rad = thehta_refracción_n2 * (Math.PI / 180);
  let n = (n10 / n11) * Math.sin(gra_rad);
  var ns = Math.asin(n);
  let thehta_refracción_large = ns * (180 / Math.PI);
  console.log(gra_rad);
  console.log(n);
  console.log(ns);
  console.log(thehta_refracción_large);

  //FIN de la distancia reccorida. Continuacion con el ángulo de refracción
  let multiplier = 100;
  let thehta_refracción =
    Math.round(thehta_refracción_large * multiplier) / multiplier;
  decimo2.innerHTML = thehta_refracción;

  var thehta_critico = Math.asin(n10 / n9);
  var thehta_critico_degree_large = thehta_critico * (180 / Math.PI);
  console.log(thehta_critico);
  console.log(thehta_critico_degree_large); //se imprime angulo critico valor completo
  let thehta_critico_degree =
    Math.round(thehta_critico_degree_large * multiplier) / multiplier; // se redondea el angulo critico
  deltax10.innerHTML = thehta_critico_degree;
  console.log(deltax10);

  //Distancia recorrida en cm

  /* Para obtener el texto */
  var combo = document.getElementById("e10");
  var selected = combo.options[combo.selectedIndex].text;

  console.log(selected);
  e10 = parseFloat(selected, 10);

  var cos_refracción_rad = Math.cos(Math.asin(n));
  var distancia_rad = e10 / cos_refracción_rad;
  console.log(distancia_rad);
  let distancia = Math.round(distancia_rad * multiplier) / multiplier;
  deltay10.innerHTML = distancia;
}

function strokeCircle18(x, y, r) {
  ctx10.beginPath();
  ctx10.arc(x, y, r, 0, 2 * Math.PI);
  ctx10.lineWidth = 3;
  ctx10.strokeStyle = "pink";
  ctx10.stroke();

  let thehta_inci2 = noveno2.innerHTML;
  let thehta_inci2_n = parseFloat(thehta_inci2, 10);
  console.log(thehta_inci2_n);

  var elCanvas = document.getElementById("c10");
  if (elCanvas && elCanvas.getContext) {
    var context = elCanvas.getContext("2d");
    context.beginPath();
    if (context) {
      var aPartida = (Math.PI / 180) * -90;
      var aFinal = (Math.PI / 180) * (-90 + thehta_inci2_n);

      //context.arc(x, y, r, aPartida, aFinal, false);

      var Xfinal = x + r * Math.cos(aPartida);
      var Yfinal = y + r * Math.sin(aPartida);

      ctx10.fillStyle = "#abcdef";
      ctx10.strokeStyle = "#1E90FF";
      ctx10.lineWidth = 3;

      //Dibujo:
      context.beginPath();
      context.moveTo(x, y);
      context.lineTo(Xfinal, Yfinal);
      context.arc(x, y, r, aPartida, aFinal);
      context.closePath();
      context.fill();
      context.stroke();
      console.log(thehta_inci2);
      console.log(aFinal);

      var incidente = parseFloat(noveno2.innerHTML, 10);
      var critico = parseFloat(deltax10.innerHTML, 10);
      console.log(incidente);
      console.log(critico);

      //DATOS TABLA DINAMINCA
      let nombre_materila2 = document.getElementById("coeficiente n10").value;
      material10.innerHTML = nombre_materila2;

      let valor_material2 = document.getElementById("coeficiente n10");
      let indice2 = valor_material2.options[valor_material2.selectedIndex].text;
      valor10.innerHTML = indice2;

      ángulo_incidente10.innerHTML = noveno2.innerHTML;
      ángulo_critico10.innerHTML = deltax10.innerHTML;
      ángulo_refracción10.innerHTML = decimo2.innerHTML;
      distancia10.innerHTML = deltay10.innerHTML;

      //suma de las distancias
      let dis1 = parseFloat(deltay.innerHTML);
      let dis2 = parseFloat(deltay2.innerHTML);
      let dis3 = parseFloat(deltay3.innerHTML);
      let dis4 = parseFloat(deltay4.innerHTML);
      let dis5 = parseFloat(deltay5.innerHTML);
      let dis6 = parseFloat(deltay6.innerHTML);
      let dis7 = parseFloat(deltay7.innerHTML);
      let dis8 = parseFloat(deltay8.innerHTML);
      let dis9 = parseFloat(deltay9.innerHTML);
      let dis10 = parseFloat(deltay10.innerHTML);

      let multiplier = 100;
      distancia_total.innerHTML =
        Math.round(
          (dis1 +
            dis2 +
            dis3 +
            dis4 +
            dis5 +
            dis6 +
            dis7 +
            dis8 +
            dis9 +
            dis10) *
            multiplier
        ) / multiplier;

      distancia_acumuluda10.innerHTML =
        Math.round(
          (dis1 +
            dis2 +
            dis3 +
            dis4 +
            dis5 +
            dis6 +
            dis7 +
            dis8 +
            dis9 +
            dis10) *
            multiplier
        ) / multiplier;

      //FIN TABLA DENAMICA

      if (incidente > critico) {
        // grafica del ángulo critico
        ctx10.beginPath();
        ctx10.arc(x, y, r, 0, 2 * Math.PI);
        ctx10.lineWidth = 3;
        ctx10.strokeStyle = "pink";
        ctx10.stroke();

        let thehta_inci = noveno2.innerHTML;
        let thehta_inci_n = parseFloat(thehta_inci, 10);
        console.log(thehta_inci_n);
        if (elCanvas && elCanvas.getContext) {
          context.beginPath();
          if (context) {
            var aPartida = (Math.PI / 180) * 180;
            var aFinal = (Math.PI / 180) * (-90 - thehta_inci_n);

            //context.arc(x, y, r, aPartida, aFinal, false);

            var Xfinal = x + r * Math.cos(aPartida);
            var Yfinal = y + r * Math.sin(aPartida);

            ctx10.fillStyle = "#abcdef";
            ctx10.strokeStyle = "red";
            ctx10.lineWidth = 3;

            //Dibujo:
            context.beginPath();
            context.moveTo(x, y);
            context.lineTo(Xfinal, Yfinal);
            context.arc(x, y, r, aPartida, aFinal);
            context.closePath();
            context.fill();
            context.stroke();
          }
        }
      }
    }
  }
}

//*********************************************************// */
function strokeCircle19(x, y, r) {
  ctx10.beginPath();
  ctx10.arc(x, y, r, 0, 2 * Math.PI);
  ctx10.lineWidth = 3;
  ctx10.strokeStyle = "pink";
  ctx10.stroke();

  let thehta_refra2 = noveno2.innerHTML;
  let thehta_refra2_n = parseFloat(thehta_refra2, 10);
  console.log(thehta_refra2_n);

  var elCanvas = document.getElementById("c10");
  if (elCanvas && elCanvas.getContext) {
    var context = elCanvas.getContext("2d");
    context.beginPath();
    if (context) {
      var aPartida = (Math.PI / 180) * 90;
      var aFinal = (Math.PI / 180) * (90 + thehta_refra2_n);

      //context.arc(x, y, r, aPartida, aFinal, false);

      var Xfinal = x + r * Math.cos(aPartida);
      var Yfinal = y + r * Math.sin(aPartida);

      ctx10.fillStyle = "#abcdef";
      ctx10.strokeStyle = "#1E90FF";
      ctx10.lineWidth = 3;

      //Dibujo:
      context.beginPath();
      context.moveTo(x, y);
      context.lineTo(Xfinal, Yfinal);
      context.arc(x, y, r, aPartida, aFinal);
      context.closePath();
      context.fill();
      context.stroke();
      console.log(thehta_refra2);
      console.log(aFinal);
    }
  }
}
