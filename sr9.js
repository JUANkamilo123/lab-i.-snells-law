var output9 = document.getElementById("output9");
var noveno = document.getElementById("noveno");
var noveno2 = document.getElementById("noveno2");
var deltax9 = document.getElementById("deltax9");
var deltay9 = document.getElementById("deltay9");
var c9 = document.getElementById("c9");
var ctx9 = c9.getContext("2d");

var cw = (c9.width = 300),
  cx = cw / 2;
var ch = (c9.height = 300),
  cy = ch / 2;
//var rad = Math.PI / 180;
var R = 100,
  r = 5;

var handle9 = {
  x: cx + R,
  y: cy,
  r: 5,
};
output9.style.top = handle9.y - 30 + "px";
output9.style.left = handle9.x - 30 + "px";

var isDragging = false;
ctx9.strokeStyle = "#555";
ctx9.fillStyle = "#e18728";
// circle

drawAxes9();
strokeCircle16(cx, cy, R);
strokeCircle17(cx, cy, R);
//drawHandle2(handle2);
drawHub9();

// Events ***************************

c9.addEventListener(
  "mousedown",
  function (evt) {
    isDragging = true;
    updateHandle9(evt);
  },
  false
);

// mousemove
c9.addEventListener(
  "mousemove",
  function (evt) {
    if (isDragging) {
      updateHandle9(evt);
    }
  },
  false
);
// mouseup
c9.addEventListener(
  "mouseup",
  function () {
    isDragging = false;
  },
  false
);
// mouseout
c9.addEventListener(
  "mouseout",
  function (evt) {
    isDragging = false;
  },
  false
); /**/

// Helpers ***************************
function strokeCircle16(x, y, r) {
  ctx9.beginPath();
  ctx9.arc(x, y, r, 0, 2 * Math.PI);
  ctx9.lineWidth = 3;
  ctx9.strokeStyle = "black";
  ctx9.stroke();

  var elCanvas9 = document.getElementById("c9");
  if (elCanvas9 && elCanvas9.getContext) {
    var context9 = elCanvas9.getContext("2d");
    context9.beginPath();
    if (context9) {
      var aPartida9 = (Math.PI / 180) * 270;
      var aFinal9 = (Math.PI / 180) * 320;
      context9.strokeStyle = "blue";
      context9.lineWidth = 3;
      context9.arc(x, y, r, aPartida9, aFinal9, false);
      context9.stroke();
    }
  }
}

function strokeCircle17(x, y, r) {
  ctx9.beginPath();
  ctx9.arc(x, y, r, 0, 2 * Math.PI);
  ctx9.lineWidth = 3;
  ctx9.strokeStyle = "black";
  ctx9.stroke();

  var elCanvas9 = document.getElementById("c9");
  if (elCanvas9 && elCanvas9.getContext) {
    var context9 = elCanvas9.getContext("2d");
    context9.beginPath();
    if (context9) {
      var aPartida9 = (Math.PI / 180) * 270;
      var aFinal9 = (Math.PI / 180) * 320;
      context9.strokeStyle = "blue";
      context9.lineWidth = 3;
      context9.arc(x, y, r, aPartida9, aFinal9, false);
      context9.stroke();
    }
  }
}

function fillCircle9(x, y, r) {
  ctx9.beginPath();
  ctx9.arc(x, y, r, 0, 2 * Math.PI);
  ctx9.save();
  ctx9.strokeStyle = "#ccc";
  ctx9.lineWidth = 3;
  ctx9.fill();
  ctx9.stroke();
  ctx9.restore();
}

function drawHub9() {
  ctx9.save();
  ctx9.fillStyle = "black";
  fillCircle9(cx, cy, 3);
  ctx9.restore();
}

function drawAxes9() {
  ctx9.save();
  ctx9.setLineDash([4, 4]);
  ctx9.strokeStyle = "#ccc";
  ctx9.beginPath();
  ctx9.moveTo(cx, 0);
  ctx9.lineTo(cx, ch); //Recta eje vertical
  ctx9.moveTo(0, cy); // Recta eje horizontal
  ctx9.lineTo(cw, cy);
  ctx9.stroke();
  ctx9.restore();
}

function oMousePos9(canvas, evt) {
  var rect = canvas.getBoundingClientRect();
  return {
    x: Math.round(evt.clientX - rect.left),
    y: Math.round(evt.clientY - rect.top),
  };
}

function updateHandle9(evt) {
  var m = oMousePos9(c9, evt);
  var deltaX = m.x - cx;
  var deltaY = m.y - cy;
  handle9.a = Math.atan2(deltaY, deltaX);
  handle9.b = Math.atan(deltaY / deltaX);
  handle9.x = cx + R * Math.cos(handle9.a);
  handle9.y = cy + R * Math.sin(handle9.a);
  ctx9.clearRect(0, 0, cw, ch);

  drawAxes9();
  strokeCircle16(cx, cy, R);
  strokeCircle17(cx, cy, R);
  //drawHandle2(handle);
  drawHub9();

  output9.innerHTML = octavo2.innerHTML + "°";
  noveno.innerHTML = octavo2.innerHTML;

  var thehta_refracción_n2 = document.getElementById("octavo2").innerHTML;
  console.log(thehta_refracción_n2);

  /* Para obtener el valor */
  var nombre1 = document.getElementById("coeficiente n9").value;
  console.log(nombre1);
  var nombre2 = document.getElementById("coeficiente n10").value;
  console.log(nombre2);

  /* Para obtener el texto */
  var combo1 = document.getElementById("coeficiente n9");
  var selected1 = combo1.options[combo1.selectedIndex].text;
  var combo2 = document.getElementById("coeficiente n10");
  var selected2 = combo2.options[combo2.selectedIndex].text;

  console.log(selected1);
  console.log(selected2);
  n9 = parseFloat(selected1, 10);
  n10 = parseFloat(selected2, 10);

  let gra_rad = thehta_refracción_n2 * (Math.PI / 180);
  let n = (n9 / n10) * Math.sin(gra_rad);
  var ns = Math.asin(n);
  let thehta_refracción_large = ns * (180 / Math.PI);
  console.log(gra_rad);
  console.log(n);
  console.log(ns);
  console.log(thehta_refracción_large);

  //FIN de la distancia reccorida. Continuacion con el ángulo de refracción
  let multiplier = 100;
  let thehta_refracción =
    Math.round(thehta_refracción_large * multiplier) / multiplier;
  noveno2.innerHTML = thehta_refracción;

  var thehta_critico = Math.asin(n10 / n9);
  var thehta_critico_degree_large = thehta_critico * (180 / Math.PI);
  console.log(thehta_critico);
  console.log(thehta_critico_degree_large); //se imprime angulo critico valor completo
  let thehta_critico_degree =
    Math.round(thehta_critico_degree_large * multiplier) / multiplier; // se redondea el angulo critico
  deltax9.innerHTML = thehta_critico_degree;
  console.log(deltax9);

  //Distancia recorrida en cm

  /* Para obtener el texto */
  var combo = document.getElementById("e9");
  var selected = combo.options[combo.selectedIndex].text;

  console.log(selected);
  e9 = parseFloat(selected, 10);

  var cos_refracción_rad = Math.cos(Math.asin(n));
  var distancia_rad = e9 / cos_refracción_rad;
  console.log(distancia_rad);
  let distancia = Math.round(distancia_rad * multiplier) / multiplier;
  deltay9.innerHTML = distancia;
}

function strokeCircle16(x, y, r) {
  ctx9.beginPath();
  ctx9.arc(x, y, r, 0, 2 * Math.PI);
  ctx9.lineWidth = 3;
  ctx9.strokeStyle = "pink";
  ctx9.stroke();

  let thehta_inci2 = octavo2.innerHTML;
  let thehta_inci2_n = parseFloat(thehta_inci2, 10);
  console.log(thehta_inci2_n);

  var elCanvas = document.getElementById("c9");
  if (elCanvas && elCanvas.getContext) {
    var context = elCanvas.getContext("2d");
    context.beginPath();
    if (context) {
      var aPartida = (Math.PI / 180) * -90;
      var aFinal = (Math.PI / 180) * (-90 + thehta_inci2_n);

      //context.arc(x, y, r, aPartida, aFinal, false);

      var Xfinal = x + r * Math.cos(aPartida);
      var Yfinal = y + r * Math.sin(aPartida);

      ctx9.fillStyle = "#abcdef";
      ctx9.strokeStyle = "#1E90FF";
      ctx9.lineWidth = 3;

      //Dibujo:
      context.beginPath();
      context.moveTo(x, y);
      context.lineTo(Xfinal, Yfinal);
      context.arc(x, y, r, aPartida, aFinal);
      context.closePath();
      context.fill();
      context.stroke();
      console.log(thehta_inci2);
      console.log(aFinal);

      var incidente = parseFloat(octavo2.innerHTML, 10);
      var critico = parseFloat(deltax9.innerHTML, 10);
      console.log(incidente);
      console.log(critico);

      //DATOS TABLA DINAMINCA
      let nombre_materila2 = document.getElementById("coeficiente n9").value;
      material9.innerHTML = nombre_materila2;

      let valor_material2 = document.getElementById("coeficiente n9");
      let indice2 = valor_material2.options[valor_material2.selectedIndex].text;
      valor9.innerHTML = indice2;

      ángulo_incidente9.innerHTML = octavo2.innerHTML;
      ángulo_critico9.innerHTML = deltax9.innerHTML;
      ángulo_refracción9.innerHTML = noveno2.innerHTML;
      distancia9.innerHTML = deltay9.innerHTML;

      //suma de las distancias
      let dis1 = parseFloat(deltay.innerHTML);
      let dis2 = parseFloat(deltay2.innerHTML);
      let dis3 = parseFloat(deltay3.innerHTML);
      let dis4 = parseFloat(deltay4.innerHTML);
      let dis5 = parseFloat(deltay5.innerHTML);
      let dis6 = parseFloat(deltay6.innerHTML);
      let dis7 = parseFloat(deltay7.innerHTML);
      let dis8 = parseFloat(deltay8.innerHTML);
      let dis9 = parseFloat(deltay9.innerHTML);
      let dis10 = parseFloat(deltay10.innerHTML);

      let multiplier = 100;
      distancia_total.innerHTML =
        Math.round(
          (dis1 +
            dis2 +
            dis3 +
            dis4 +
            dis5 +
            dis6 +
            dis7 +
            dis8 +
            dis9 +
            dis10) *
            multiplier
        ) / multiplier;

      distancia_acumuluda9.innerHTML =
        Math.round(
          (dis1 + dis2 + dis3 + dis4 + dis5 + dis6 + dis7 + dis8 + dis9) *
            multiplier
        ) / multiplier;
      //FIN TABLA DENAMICA

      if (incidente > critico) {
        // grafica del ángulo critico
        ctx9.beginPath();
        ctx9.arc(x, y, r, 0, 2 * Math.PI);
        ctx9.lineWidth = 3;
        ctx9.strokeStyle = "pink";
        ctx9.stroke();

        let thehta_inci = octavo2.innerHTML;
        let thehta_inci_n = parseFloat(thehta_inci, 10);
        console.log(thehta_inci_n);
        if (elCanvas && elCanvas.getContext) {
          context.beginPath();
          if (context) {
            var aPartida = (Math.PI / 180) * 180;
            var aFinal = (Math.PI / 180) * (-90 - thehta_inci_n);

            //context.arc(x, y, r, aPartida, aFinal, false);

            var Xfinal = x + r * Math.cos(aPartida);
            var Yfinal = y + r * Math.sin(aPartida);

            ctx9.fillStyle = "#abcdef";
            ctx9.strokeStyle = "red";
            ctx9.lineWidth = 3;

            //Dibujo:
            context.beginPath();
            context.moveTo(x, y);
            context.lineTo(Xfinal, Yfinal);
            context.arc(x, y, r, aPartida, aFinal);
            context.closePath();
            context.fill();
            context.stroke();
          }
        }
      }
    }
  }
}

//*********************************************************// */
function strokeCircle17(x, y, r) {
  ctx9.beginPath();
  ctx9.arc(x, y, r, 0, 2 * Math.PI);
  ctx9.lineWidth = 3;
  ctx9.strokeStyle = "pink";
  ctx9.stroke();

  let thehta_refra2 = noveno2.innerHTML;
  let thehta_refra2_n = parseFloat(thehta_refra2, 10);
  console.log(thehta_refra2_n);

  var elCanvas = document.getElementById("c9");
  if (elCanvas && elCanvas.getContext) {
    var context = elCanvas.getContext("2d");
    context.beginPath();
    if (context) {
      var aPartida = (Math.PI / 180) * 90;
      var aFinal = (Math.PI / 180) * (90 + thehta_refra2_n);

      //context.arc(x, y, r, aPartida, aFinal, false);

      var Xfinal = x + r * Math.cos(aPartida);
      var Yfinal = y + r * Math.sin(aPartida);

      ctx9.fillStyle = "#abcdef";
      ctx9.strokeStyle = "#1E90FF";
      ctx9.lineWidth = 3;

      //Dibujo:
      context.beginPath();
      context.moveTo(x, y);
      context.lineTo(Xfinal, Yfinal);
      context.arc(x, y, r, aPartida, aFinal);
      context.closePath();
      context.fill();
      context.stroke();
      console.log(thehta_refra2);
      console.log(aFinal);
    }
  }
}
