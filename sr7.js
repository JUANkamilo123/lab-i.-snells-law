var output7 = document.getElementById("output7");
var septimo = document.getElementById("septimo");
var septimo2 = document.getElementById("septimo2");
var deltax7 = document.getElementById("deltax7");
var deltay7 = document.getElementById("deltay7");
var c7 = document.getElementById("c7");
var ctx7 = c7.getContext("2d");

var cw = (c7.width = 300),
  cx = cw / 2;
var ch = (c7.height = 300),
  cy = ch / 2;
//var rad = Math.PI / 180;
var R = 100,
  r = 5;

var handle7 = {
  x: cx + R,
  y: cy,
  r: 5,
};
output7.style.top = handle7.y - 30 + "px";
output7.style.left = handle7.x - 30 + "px";

var isDragging = false;
ctx7.strokeStyle = "#555";
ctx7.fillStyle = "#e18728";
// circle

drawAxes7();
strokeCircle12(cx, cy, R);
strokeCircle13(cx, cy, R);
//drawHandle2(handle2);
drawHub7();

// Events ***************************

c7.addEventListener(
  "mousedown",
  function (evt) {
    isDragging = true;
    updateHandle7(evt);
  },
  false
);

// mousemove
c7.addEventListener(
  "mousemove",
  function (evt) {
    if (isDragging) {
      updateHandle7(evt);
    }
  },
  false
);
// mouseup
c7.addEventListener(
  "mouseup",
  function () {
    isDragging = false;
  },
  false
);
// mouseout
c7.addEventListener(
  "mouseout",
  function (evt) {
    isDragging = false;
  },
  false
); /**/

// Helpers ***************************
function strokeCircle12(x, y, r) {
  ctx7.beginPath();
  ctx7.arc(x, y, r, 0, 2 * Math.PI);
  ctx7.lineWidth = 3;
  ctx7.strokeStyle = "black";
  ctx7.stroke();

  var elCanvas7 = document.getElementById("c7");
  if (elCanvas7 && elCanvas7.getContext) {
    var context7 = elCanvas7.getContext("2d");
    context7.beginPath();
    if (context7) {
      var aPartida7 = (Math.PI / 180) * 270;
      var aFinal7 = (Math.PI / 180) * 320;
      context7.strokeStyle = "blue";
      context7.lineWidth = 3;
      context7.arc(x, y, r, aPartida7, aFinal7, false);
      context7.stroke();
    }
  }
}

function strokeCircle13(x, y, r) {
  ctx7.beginPath();
  ctx7.arc(x, y, r, 0, 2 * Math.PI);
  ctx7.lineWidth = 3;
  ctx7.strokeStyle = "black";
  ctx7.stroke();

  var elCanvas7 = document.getElementById("c7");
  if (elCanvas7 && elCanvas7.getContext) {
    var context7 = elCanvas7.getContext("2d");
    context7.beginPath();
    if (context7) {
      var aPartida7 = (Math.PI / 180) * 270;
      var aFinal7 = (Math.PI / 180) * 320;
      context7.strokeStyle = "blue";
      context7.lineWidth = 3;
      context7.arc(x, y, r, aPartida7, aFinal7, false);
      context7.stroke();
    }
  }
}

function fillCircle7(x, y, r) {
  ctx7.beginPath();
  ctx7.arc(x, y, r, 0, 2 * Math.PI);
  ctx7.save();
  ctx7.strokeStyle = "#ccc";
  ctx7.lineWidth = 3;
  ctx7.fill();
  ctx7.stroke();
  ctx7.restore();
}

function drawHub7() {
  ctx7.save();
  ctx7.fillStyle = "black";
  fillCircle7(cx, cy, 3);
  ctx7.restore();
}

function drawAxes7() {
  ctx7.save();
  ctx7.setLineDash([4, 4]);
  ctx7.strokeStyle = "#ccc";
  ctx7.beginPath();
  ctx7.moveTo(cx, 0);
  ctx7.lineTo(cx, ch); //Recta eje vertical
  ctx7.moveTo(0, cy); // Recta eje horizontal
  ctx7.lineTo(cw, cy);
  ctx7.stroke();
  ctx7.restore();
}

function oMousePos7(canvas, evt) {
  var rect = canvas.getBoundingClientRect();
  return {
    x: Math.round(evt.clientX - rect.left),
    y: Math.round(evt.clientY - rect.top),
  };
}

function updateHandle7(evt) {
  var m = oMousePos5(c7, evt);
  var deltaX = m.x - cx;
  var deltaY = m.y - cy;
  handle7.a = Math.atan2(deltaY, deltaX);
  handle7.b = Math.atan(deltaY / deltaX);
  handle7.x = cx + R * Math.cos(handle7.a);
  handle7.y = cy + R * Math.sin(handle7.a);
  ctx7.clearRect(0, 0, cw, ch);

  drawAxes7();
  strokeCircle12(cx, cy, R);
  strokeCircle13(cx, cy, R);
  //drawHandle2(handle);
  drawHub7();

  output7.innerHTML = sexto2.innerHTML + "°";
  septimo.innerHTML = sexto2.innerHTML;

  var thehta_refracción_n2 = document.getElementById("sexto2").innerHTML;
  console.log(thehta_refracción_n2);

  /* Para obtener el valor */
  var nombre1 = document.getElementById("coeficiente n7").value;
  console.log(nombre1);
  var nombre2 = document.getElementById("coeficiente n8").value;
  console.log(nombre2);

  /* Para obtener el texto */
  var combo1 = document.getElementById("coeficiente n7");
  var selected1 = combo1.options[combo1.selectedIndex].text;
  var combo2 = document.getElementById("coeficiente n8");
  var selected2 = combo2.options[combo2.selectedIndex].text;

  console.log(selected1);
  console.log(selected2);
  n7 = parseFloat(selected1, 10);
  n8 = parseFloat(selected2, 10);

  let gra_rad = thehta_refracción_n2 * (Math.PI / 180);
  let n = (n7 / n8) * Math.sin(gra_rad);
  var ns = Math.asin(n);
  let thehta_refracción_large = ns * (180 / Math.PI);
  console.log(gra_rad);
  console.log(n);
  console.log(ns);
  console.log(thehta_refracción_large);

  //FIN de la distancia reccorida. Continuacion con el ángulo de refracción
  let multiplier = 100;
  let thehta_refracción =
    Math.round(thehta_refracción_large * multiplier) / multiplier;
  septimo2.innerHTML = thehta_refracción;

  var thehta_critico = Math.asin(n8 / n7);
  var thehta_critico_degree_large = thehta_critico * (180 / Math.PI);
  console.log(thehta_critico);
  console.log(thehta_critico_degree_large); //se imprime angulo critico valor completo
  let thehta_critico_degree =
    Math.round(thehta_critico_degree_large * multiplier) / multiplier; // se redondea el angulo critico
  deltax7.innerHTML = thehta_critico_degree;
  console.log(deltax7);

  //Distancia recorrida en cm

  /* Para obtener el texto */
  var combo = document.getElementById("e7");
  var selected = combo.options[combo.selectedIndex].text;

  console.log(selected);
  e7 = parseFloat(selected, 10);

  var cos_refracción_rad = Math.cos(Math.asin(n));
  var distancia_rad = e7 / cos_refracción_rad;
  console.log(distancia_rad);
  let distancia = Math.round(distancia_rad * multiplier) / multiplier;
  deltay7.innerHTML = distancia;
}

function strokeCircle12(x, y, r) {
  ctx7.beginPath();
  ctx7.arc(x, y, r, 0, 2 * Math.PI);
  ctx7.lineWidth = 3;
  ctx7.strokeStyle = "pink";
  ctx7.stroke();

  let thehta_inci2 = sexto2.innerHTML;
  let thehta_inci2_n = parseFloat(thehta_inci2, 10);
  console.log(thehta_inci2_n);

  var elCanvas = document.getElementById("c7");
  if (elCanvas && elCanvas.getContext) {
    var context = elCanvas.getContext("2d");
    context.beginPath();
    if (context) {
      var aPartida = (Math.PI / 180) * -90;
      var aFinal = (Math.PI / 180) * (-90 + thehta_inci2_n);

      //context.arc(x, y, r, aPartida, aFinal, false);

      var Xfinal = x + r * Math.cos(aPartida);
      var Yfinal = y + r * Math.sin(aPartida);

      ctx7.fillStyle = "#abcdef";
      ctx7.strokeStyle = "#1E90FF";
      ctx7.lineWidth = 3;

      //Dibujo:
      context.beginPath();
      context.moveTo(x, y);
      context.lineTo(Xfinal, Yfinal);
      context.arc(x, y, r, aPartida, aFinal);
      context.closePath();
      context.fill();
      context.stroke();
      console.log(thehta_inci2);
      console.log(aFinal);

      var incidente = parseFloat(sexto2.innerHTML, 10);
      var critico = parseFloat(deltax7.innerHTML, 10);
      console.log(incidente);
      console.log(critico);

      //DATOS TABLA DINAMINCA
      let nombre_materila2 = document.getElementById("coeficiente n7").value;
      material7.innerHTML = nombre_materila2;

      let valor_material2 = document.getElementById("coeficiente n7");
      let indice2 = valor_material2.options[valor_material2.selectedIndex].text;
      valor7.innerHTML = indice2;

      ángulo_incidente7.innerHTML = sexto2.innerHTML;
      ángulo_critico7.innerHTML = deltax7.innerHTML;
      ángulo_refracción7.innerHTML = septimo2.innerHTML;
      distancia7.innerHTML = deltay7.innerHTML;

      //suma de las distancias
      let dis1 = parseFloat(deltay.innerHTML);
      let dis2 = parseFloat(deltay2.innerHTML);
      let dis3 = parseFloat(deltay3.innerHTML);
      let dis4 = parseFloat(deltay4.innerHTML);
      let dis5 = parseFloat(deltay5.innerHTML);
      let dis6 = parseFloat(deltay6.innerHTML);
      let dis7 = parseFloat(deltay7.innerHTML);
      let dis8 = parseFloat(deltay8.innerHTML);
      let dis9 = parseFloat(deltay9.innerHTML);
      let dis10 = parseFloat(deltay10.innerHTML);
      let multiplier = 100;
      distancia_total.innerHTML =
        Math.round(
          (dis1 +
            dis2 +
            dis3 +
            dis4 +
            dis5 +
            dis6 +
            dis7 +
            dis8 +
            dis9 +
            dis10) *
            multiplier
        ) / multiplier;

      distancia_acumuluda7.innerHTML =
        Math.round(
          (dis1 + dis2 + dis3 + dis4 + dis5 + dis6 + dis7) * multiplier
        ) / multiplier;

      //FIN TABLA DENAMICA

      if (incidente > critico) {
        // grafica del ángulo critico
        ctx7.beginPath();
        ctx7.arc(x, y, r, 0, 2 * Math.PI);
        ctx7.lineWidth = 3;
        ctx7.strokeStyle = "pink";
        ctx7.stroke();

        let thehta_inci = sexto2.innerHTML;
        let thehta_inci_n = parseFloat(thehta_inci, 10);
        console.log(thehta_inci_n);
        if (elCanvas && elCanvas.getContext) {
          context.beginPath();
          if (context) {
            var aPartida = (Math.PI / 180) * 180;
            var aFinal = (Math.PI / 180) * (-90 - thehta_inci_n);

            //context.arc(x, y, r, aPartida, aFinal, false);

            var Xfinal = x + r * Math.cos(aPartida);
            var Yfinal = y + r * Math.sin(aPartida);

            ctx7.fillStyle = "#abcdef";
            ctx7.strokeStyle = "red";
            ctx7.lineWidth = 3;

            //Dibujo:
            context.beginPath();
            context.moveTo(x, y);
            context.lineTo(Xfinal, Yfinal);
            context.arc(x, y, r, aPartida, aFinal);
            context.closePath();
            context.fill();
            context.stroke();
          }
        }
      }
    }
  }
}

//*********************************************************// */
function strokeCircle13(x, y, r) {
  ctx7.beginPath();
  ctx7.arc(x, y, r, 0, 2 * Math.PI);
  ctx7.lineWidth = 3;
  ctx7.strokeStyle = "pink";
  ctx7.stroke();

  let thehta_refra2 = septimo2.innerHTML;
  let thehta_refra2_n = parseFloat(thehta_refra2, 10);
  console.log(thehta_refra2_n);

  var elCanvas = document.getElementById("c7");
  if (elCanvas && elCanvas.getContext) {
    var context = elCanvas.getContext("2d");
    context.beginPath();
    if (context) {
      var aPartida = (Math.PI / 180) * 90;
      var aFinal = (Math.PI / 180) * (90 + thehta_refra2_n);

      //context.arc(x, y, r, aPartida, aFinal, false);

      var Xfinal = x + r * Math.cos(aPartida);
      var Yfinal = y + r * Math.sin(aPartida);

      ctx7.fillStyle = "#abcdef";
      ctx7.strokeStyle = "#1E90FF";
      ctx7.lineWidth = 3;

      //Dibujo:
      context.beginPath();
      context.moveTo(x, y);
      context.lineTo(Xfinal, Yfinal);
      context.arc(x, y, r, aPartida, aFinal);
      context.closePath();
      context.fill();
      context.stroke();
      console.log(thehta_refra2);
      console.log(aFinal);
    }
  }
}
