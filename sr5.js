var output5 = document.getElementById("output5");
var quinto = document.getElementById("quinto");
var quinto2 = document.getElementById("quinto2");
var deltax5 = document.getElementById("deltax5");
var deltay5 = document.getElementById("deltay5");
var c5 = document.getElementById("c5");
var ctx5 = c5.getContext("2d");

var cw = (c5.width = 300),
  cx = cw / 2;
var ch = (c5.height = 300),
  cy = ch / 2;
//var rad = Math.PI / 180;
var R = 100,
  r = 5;

var handle5 = {
  x: cx + R,
  y: cy,
  r: 5,
};
output5.style.top = handle5.y - 30 + "px";
output5.style.left = handle5.x - 30 + "px";

var isDragging = false;
ctx5.strokeStyle = "#555";
ctx5.fillStyle = "#e18728";
// circle

drawAxes5();
strokeCircle8(cx, cy, R);
strokeCircle9(cx, cy, R);
//drawHandle2(handle2);
drawHub5();

// Events ***************************

c5.addEventListener(
  "mousedown",
  function (evt) {
    isDragging = true;
    updateHandle5(evt);
  },
  false
);

// mousemove
c5.addEventListener(
  "mousemove",
  function (evt) {
    if (isDragging) {
      updateHandle5(evt);
    }
  },
  false
);
// mouseup
c5.addEventListener(
  "mouseup",
  function () {
    isDragging = false;
  },
  false
);
// mouseout
c5.addEventListener(
  "mouseout",
  function (evt) {
    isDragging = false;
  },
  false
); /**/

// Helpers ***************************
function strokeCircle8(x, y, r) {
  ctx5.beginPath();
  ctx5.arc(x, y, r, 0, 2 * Math.PI);
  ctx5.lineWidth = 3;
  ctx5.strokeStyle = "black";
  ctx5.stroke();

  var elCanvas5 = document.getElementById("c5");
  if (elCanvas5 && elCanvas5.getContext) {
    var context5 = elCanvas5.getContext("2d");
    context5.beginPath();
    if (context5) {
      var aPartida5 = (Math.PI / 180) * 270;
      var aFinal5 = (Math.PI / 180) * 320;
      context5.strokeStyle = "blue";
      context5.lineWidth = 3;
      context5.arc(x, y, r, aPartida5, aFinal5, false);
      context5.stroke();
    }
  }
}

function strokeCircle9(x, y, r) {
  ctx5.beginPath();
  ctx5.arc(x, y, r, 0, 2 * Math.PI);
  ctx5.lineWidth = 3;
  ctx5.strokeStyle = "black";
  ctx5.stroke();

  var elCanvas5 = document.getElementById("c5");
  if (elCanvas5 && elCanvas5.getContext) {
    var context5 = elCanvas5.getContext("2d");
    context5.beginPath();
    if (context5) {
      var aPartida5 = (Math.PI / 180) * 270;
      var aFinal5 = (Math.PI / 180) * 320;
      context5.strokeStyle = "blue";
      context5.lineWidth = 3;
      context5.arc(x, y, r, aPartida5, aFinal5, false);
      context5.stroke();
    }
  }
}

function fillCircle5(x, y, r) {
  ctx5.beginPath();
  ctx5.arc(x, y, r, 0, 2 * Math.PI);
  ctx5.save();
  ctx5.strokeStyle = "#ccc";
  ctx5.lineWidth = 3;
  ctx5.fill();
  ctx5.stroke();
  ctx5.restore();
}

function drawHub5() {
  ctx5.save();
  ctx5.fillStyle = "black";
  fillCircle5(cx, cy, 3);
  ctx5.restore();
}

function drawAxes5() {
  ctx5.save();
  ctx5.setLineDash([4, 4]);
  ctx5.strokeStyle = "#ccc";
  ctx5.beginPath();
  ctx5.moveTo(cx, 0);
  ctx5.lineTo(cx, ch); //Recta eje vertical
  ctx5.moveTo(0, cy); // Recta eje horizontal
  ctx5.lineTo(cw, cy);
  ctx5.stroke();
  ctx5.restore();
}

function oMousePos5(canvas, evt) {
  var rect = canvas.getBoundingClientRect();
  return {
    x: Math.round(evt.clientX - rect.left),
    y: Math.round(evt.clientY - rect.top),
  };
}

function updateHandle5(evt) {
  var m = oMousePos5(c5, evt);
  var deltaX = m.x - cx;
  var deltaY = m.y - cy;
  handle5.a = Math.atan2(deltaY, deltaX);
  handle5.b = Math.atan(deltaY / deltaX);
  handle5.x = cx + R * Math.cos(handle5.a);
  handle5.y = cy + R * Math.sin(handle5.a);
  ctx5.clearRect(0, 0, cw, ch);

  drawAxes5();
  strokeCircle8(cx, cy, R);
  strokeCircle9(cx, cy, R);
  //drawHandle2(handle);
  drawHub5();

  output5.innerHTML = cuarto2.innerHTML + "°";
  quinto.innerHTML = cuarto2.innerHTML;

  var thehta_refracción_n2 = document.getElementById("cuarto2").innerHTML;
  console.log(thehta_refracción_n2);

  /* Para obtener el valor */
  var nombre1 = document.getElementById("coeficiente n5").value;
  console.log(nombre1);
  var nombre2 = document.getElementById("coeficiente n6").value;
  console.log(nombre2);

  /* Para obtener el texto */
  var combo1 = document.getElementById("coeficiente n5");
  var selected1 = combo1.options[combo1.selectedIndex].text;
  var combo2 = document.getElementById("coeficiente n6");
  var selected2 = combo2.options[combo2.selectedIndex].text;

  console.log(selected1);
  console.log(selected2);
  n5 = parseFloat(selected1, 10);
  n6 = parseFloat(selected2, 10);

  let gra_rad = thehta_refracción_n2 * (Math.PI / 180);
  let n = (n5 / n6) * Math.sin(gra_rad);
  var ns = Math.asin(n);
  let thehta_refracción_large = ns * (180 / Math.PI);
  console.log(gra_rad);
  console.log(n);
  console.log(ns);
  console.log(thehta_refracción_large);

  //FIN de la distancia reccorida. Continuacion con el ángulo de refracción
  let multiplier = 100;
  let thehta_refracción =
    Math.round(thehta_refracción_large * multiplier) / multiplier;
  quinto2.innerHTML = thehta_refracción;

  var thehta_critico = Math.asin(n6 / n5);
  var thehta_critico_degree_large = thehta_critico * (180 / Math.PI);
  console.log(thehta_critico);
  console.log(thehta_critico_degree_large); //se imprime angulo critico valor completo
  let thehta_critico_degree =
    Math.round(thehta_critico_degree_large * multiplier) / multiplier; // se redondea el angulo critico
  deltax5.innerHTML = thehta_critico_degree;
  console.log(deltax5);

  //Distancia recorrida en cm

  /* Para obtener el texto */
  var combo = document.getElementById("e5");
  var selected = combo.options[combo.selectedIndex].text;

  console.log(selected);
  e5 = parseFloat(selected, 10);

  var cos_refracción_rad = Math.cos(Math.asin(n));
  var distancia_rad = e5 / cos_refracción_rad;
  console.log(distancia_rad);
  let distancia = Math.round(distancia_rad * multiplier) / multiplier;
  deltay5.innerHTML = distancia;
}

function strokeCircle8(x, y, r) {
  ctx5.beginPath();
  ctx5.arc(x, y, r, 0, 2 * Math.PI);
  ctx5.lineWidth = 3;
  ctx5.strokeStyle = "pink";
  ctx5.stroke();

  let thehta_inci2 = cuarto2.innerHTML;
  let thehta_inci2_n = parseFloat(thehta_inci2, 10);
  console.log(thehta_inci2_n);

  var elCanvas = document.getElementById("c5");
  if (elCanvas && elCanvas.getContext) {
    var context = elCanvas.getContext("2d");
    context.beginPath();
    if (context) {
      var aPartida = (Math.PI / 180) * -90;
      var aFinal = (Math.PI / 180) * (-90 + thehta_inci2_n);

      //context.arc(x, y, r, aPartida, aFinal, false);

      var Xfinal = x + r * Math.cos(aPartida);
      var Yfinal = y + r * Math.sin(aPartida);

      ctx5.fillStyle = "#abcdef";
      ctx5.strokeStyle = "#1E90FF";
      ctx5.lineWidth = 3;

      //Dibujo:
      context.beginPath();
      context.moveTo(x, y);
      context.lineTo(Xfinal, Yfinal);
      context.arc(x, y, r, aPartida, aFinal);
      context.closePath();
      context.fill();
      context.stroke();
      console.log(thehta_inci2);
      console.log(aFinal);

      var incidente = parseFloat(cuarto2.innerHTML, 10);
      var critico = parseFloat(deltax5.innerHTML, 10);
      console.log(incidente);
      console.log(critico);

      //DATOS TABLA DINAMINCA
      let nombre_materila2 = document.getElementById("coeficiente n5").value;
      material5.innerHTML = nombre_materila2;

      let valor_material2 = document.getElementById("coeficiente n5");
      let indice2 = valor_material2.options[valor_material2.selectedIndex].text;
      valor5.innerHTML = indice2;

      ángulo_incidente5.innerHTML = cuarto2.innerHTML;
      ángulo_critico5.innerHTML = deltax5.innerHTML;
      ángulo_refracción5.innerHTML = quinto2.innerHTML;
      distancia5.innerHTML = deltay5.innerHTML;

      //suma de las distancias
      let dis1 = parseFloat(deltay.innerHTML);
      let dis2 = parseFloat(deltay2.innerHTML);
      let dis3 = parseFloat(deltay3.innerHTML);
      let dis4 = parseFloat(deltay4.innerHTML);
      let dis5 = parseFloat(deltay5.innerHTML);
      let dis6 = parseFloat(deltay6.innerHTML);
      let dis7 = parseFloat(deltay7.innerHTML);
      let dis8 = parseFloat(deltay8.innerHTML);
      let dis9 = parseFloat(deltay9.innerHTML);
      let dis10 = parseFloat(deltay10.innerHTML);

      let multiplier = 100;
      distancia_total.innerHTML =
        Math.round(
          (dis1 +
            dis2 +
            dis3 +
            dis4 +
            dis5 +
            dis6 +
            dis7 +
            dis8 +
            dis9 +
            dis10) *
            multiplier
        ) / multiplier;

      distancia_acumuluda5.innerHTML =
        Math.round((dis1 + dis2 + dis3 + dis4 + dis5) * multiplier) /
        multiplier;

      //FIN TABLA DENAMICA

      if (incidente > critico) {
        // grafica del ángulo critico
        ctx5.beginPath();
        ctx5.arc(x, y, r, 0, 2 * Math.PI);
        ctx5.lineWidth = 3;
        ctx5.strokeStyle = "pink";
        ctx5.stroke();

        let thehta_inci = cuarto2.innerHTML;
        let thehta_inci_n = parseFloat(thehta_inci, 10);
        console.log(thehta_inci_n);
        if (elCanvas && elCanvas.getContext) {
          context.beginPath();
          if (context) {
            var aPartida = (Math.PI / 180) * 180;
            var aFinal = (Math.PI / 180) * (-90 - thehta_inci_n);

            //context.arc(x, y, r, aPartida, aFinal, false);

            var Xfinal = x + r * Math.cos(aPartida);
            var Yfinal = y + r * Math.sin(aPartida);

            ctx5.fillStyle = "#abcdef";
            ctx5.strokeStyle = "red";
            ctx5.lineWidth = 3;

            //Dibujo:
            context.beginPath();
            context.moveTo(x, y);
            context.lineTo(Xfinal, Yfinal);
            context.arc(x, y, r, aPartida, aFinal);
            context.closePath();
            context.fill();
            context.stroke();
          }
        }
      }
    }
  }
}

//*********************************************************// */
function strokeCircle9(x, y, r) {
  ctx5.beginPath();
  ctx5.arc(x, y, r, 0, 2 * Math.PI);
  ctx5.lineWidth = 3;
  ctx5.strokeStyle = "pink";
  ctx5.stroke();

  let thehta_refra2 = quinto2.innerHTML;
  let thehta_refra2_n = parseFloat(thehta_refra2, 10);
  console.log(thehta_refra2_n);

  var elCanvas = document.getElementById("c5");
  if (elCanvas && elCanvas.getContext) {
    var context = elCanvas.getContext("2d");
    context.beginPath();
    if (context) {
      var aPartida = (Math.PI / 180) * 90;
      var aFinal = (Math.PI / 180) * (90 + thehta_refra2_n);

      //context.arc(x, y, r, aPartida, aFinal, false);

      var Xfinal = x + r * Math.cos(aPartida);
      var Yfinal = y + r * Math.sin(aPartida);

      ctx5.fillStyle = "#abcdef";
      ctx5.strokeStyle = "#1E90FF";
      ctx5.lineWidth = 3;

      //Dibujo:
      context.beginPath();
      context.moveTo(x, y);
      context.lineTo(Xfinal, Yfinal);
      context.arc(x, y, r, aPartida, aFinal);
      context.closePath();
      context.fill();
      context.stroke();
      console.log(thehta_refra2);
      console.log(aFinal);
    }
  }
}
