var output4 = document.getElementById("output4");
var cuarto = document.getElementById("cuarto");
var cuarto2 = document.getElementById("cuarto2");
var deltax4 = document.getElementById("deltax4");
var deltay4 = document.getElementById("deltay4");
var c4 = document.getElementById("c4");
var ctx4 = c4.getContext("2d");

var cw = (c4.width = 300),
  cx = cw / 2;
var ch = (c4.height = 300),
  cy = ch / 2;
//var rad = Math.PI / 180;
var R = 100,
  r = 5;

var handle4 = {
  x: cx + R,
  y: cy,
  r: 5,
};
output4.style.top = handle4.y - 30 + "px";
output4.style.left = handle4.x - 30 + "px";

var isDragging = false;
ctx4.strokeStyle = "#555";
ctx4.fillStyle = "#e18728";
// circle

drawAxes4();
strokeCircle6(cx, cy, R);
strokeCircle7(cx, cy, R);
//drawHandle2(handle2);
drawHub4();

// Events ***************************

c4.addEventListener(
  "mousedown",
  function (evt) {
    isDragging = true;
    updateHandle4(evt);
  },
  false
);

// mousemove
c4.addEventListener(
  "mousemove",
  function (evt) {
    if (isDragging) {
      updateHandle4(evt);
    }
  },
  false
);
// mouseup
c4.addEventListener(
  "mouseup",
  function () {
    isDragging = false;
  },
  false
);
// mouseout
c4.addEventListener(
  "mouseout",
  function (evt) {
    isDragging = false;
  },
  false
); /**/

// Helpers ***************************
function strokeCircle6(x, y, r) {
  ctx4.beginPath();
  ctx4.arc(x, y, r, 0, 2 * Math.PI);
  ctx4.lineWidth = 3;
  ctx4.strokeStyle = "black";
  ctx4.stroke();

  var elCanvas4 = document.getElementById("c4");
  if (elCanvas4 && elCanvas4.getContext) {
    var context4 = elCanvas4.getContext("2d");
    context4.beginPath();
    if (context4) {
      var aPartida4 = (Math.PI / 180) * 270;
      var aFinal4 = (Math.PI / 180) * 320;
      context4.strokeStyle = "blue";
      context4.lineWidth = 3;
      context4.arc(x, y, r, aPartida4, aFinal4, false);
      context4.stroke();
    }
  }
}

function strokeCircle7(x, y, r) {
  ctx4.beginPath();
  ctx4.arc(x, y, r, 0, 2 * Math.PI);
  ctx4.lineWidth = 3;
  ctx4.strokeStyle = "black";
  ctx4.stroke();

  var elCanvas4 = document.getElementById("c4");
  if (elCanvas4 && elCanvas4.getContext) {
    var context4 = elCanvas4.getContext("2d");
    context4.beginPath();
    if (context4) {
      var aPartida4 = (Math.PI / 180) * 270;
      var aFinal4 = (Math.PI / 180) * 320;
      context4.strokeStyle = "blue";
      context4.lineWidth = 3;
      context4.arc(x, y, r, aPartida4, aFinal4, false);
      context4.stroke();
    }
  }
}

function fillCircle4(x, y, r) {
  ctx4.beginPath();
  ctx4.arc(x, y, r, 0, 2 * Math.PI);
  ctx4.save();
  ctx4.strokeStyle = "#ccc";
  ctx4.lineWidth = 3;
  ctx4.fill();
  ctx4.stroke();
  ctx4.restore();
}

function drawHub4() {
  ctx4.save();
  ctx4.fillStyle = "black";
  fillCircle4(cx, cy, 3);
  ctx4.restore();
}

function drawAxes4() {
  ctx4.save();
  ctx4.setLineDash([4, 4]);
  ctx4.strokeStyle = "#ccc";
  ctx4.beginPath();
  ctx4.moveTo(cx, 0);
  ctx4.lineTo(cx, ch); //Recta eje vertical
  ctx4.moveTo(0, cy); // Recta eje horizontal
  ctx4.lineTo(cw, cy);
  ctx4.stroke();
  ctx4.restore();
}

function oMousePos4(canvas, evt) {
  var rect = canvas.getBoundingClientRect();
  return {
    x: Math.round(evt.clientX - rect.left),
    y: Math.round(evt.clientY - rect.top),
  };
}

function updateHandle4(evt) {
  var m = oMousePos4(c4, evt);
  var deltaX = m.x - cx;
  var deltaY = m.y - cy;
  handle4.a = Math.atan2(deltaY, deltaX);
  handle4.b = Math.atan(deltaY / deltaX);
  handle4.x = cx + R * Math.cos(handle4.a);
  handle4.y = cy + R * Math.sin(handle4.a);
  ctx4.clearRect(0, 0, cw, ch);

  drawAxes4();
  strokeCircle6(cx, cy, R);
  strokeCircle7(cx, cy, R);
  //drawHandle2(handle);
  drawHub4();

  output4.innerHTML = tercero2.innerHTML + "°";
  cuarto.innerHTML = tercero2.innerHTML;

  var thehta_refracción_n2 = document.getElementById("tercero2").innerHTML;
  console.log(thehta_refracción_n2);

  /* Para obtener el valor */
  var nombre1 = document.getElementById("coeficiente n4").value;
  console.log(nombre1);
  var nombre2 = document.getElementById("coeficiente n5").value;
  console.log(nombre2);

  /* Para obtener el texto */
  var combo1 = document.getElementById("coeficiente n4");
  var selected1 = combo1.options[combo1.selectedIndex].text;
  var combo2 = document.getElementById("coeficiente n5");
  var selected2 = combo2.options[combo2.selectedIndex].text;

  console.log(selected1);
  console.log(selected2);
  n4 = parseFloat(selected1, 10);
  n5 = parseFloat(selected2, 10);

  let gra_rad = thehta_refracción_n2 * (Math.PI / 180);
  let n = (n4 / n5) * Math.sin(gra_rad);
  var ns = Math.asin(n);
  let thehta_refracción_large = ns * (180 / Math.PI);
  console.log(gra_rad);
  console.log(n);
  console.log(ns);
  console.log(thehta_refracción_large);

  //FIN de la distancia reccorida. Continuacion con el ángulo de refracción
  let multiplier = 100;
  let thehta_refracción =
    Math.round(thehta_refracción_large * multiplier) / multiplier;
  cuarto2.innerHTML = thehta_refracción;

  var thehta_critico = Math.asin(n5 / n4);
  var thehta_critico_degree_large = thehta_critico * (180 / Math.PI);
  console.log(thehta_critico);
  console.log(thehta_critico_degree_large); //se imprime angulo critico valor completo
  let thehta_critico_degree =
    Math.round(thehta_critico_degree_large * multiplier) / multiplier; // se redondea el angulo critico
  deltax4.innerHTML = thehta_critico_degree;
  console.log(deltax4);

  //Distancia recorrida en cm

  /* Para obtener el texto */
  var combo = document.getElementById("e4");
  var selected = combo.options[combo.selectedIndex].text;

  console.log(selected);
  e4 = parseFloat(selected, 10);

  var cos_refracción_rad = Math.cos(Math.asin(n));
  var distancia_rad = e4 / cos_refracción_rad;
  console.log(distancia_rad);
  let distancia = Math.round(distancia_rad * multiplier) / multiplier;
  deltay4.innerHTML = distancia;
}

function strokeCircle6(x, y, r) {
  ctx4.beginPath();
  ctx4.arc(x, y, r, 0, 2 * Math.PI);
  ctx4.lineWidth = 3;
  ctx4.strokeStyle = "pink";
  ctx4.stroke();

  let thehta_inci2 = tercero2.innerHTML;
  let thehta_inci2_n = parseFloat(thehta_inci2, 10);
  console.log(thehta_inci2_n);

  var elCanvas = document.getElementById("c4");
  if (elCanvas && elCanvas.getContext) {
    var context = elCanvas.getContext("2d");
    context.beginPath();
    if (context) {
      var aPartida = (Math.PI / 180) * -90;
      var aFinal = (Math.PI / 180) * (-90 + thehta_inci2_n);

      //context.arc(x, y, r, aPartida, aFinal, false);

      var Xfinal = x + r * Math.cos(aPartida);
      var Yfinal = y + r * Math.sin(aPartida);

      ctx4.fillStyle = "#abcdef";
      ctx4.strokeStyle = "#1E90FF";
      ctx4.lineWidth = 3;

      //Dibujo:
      context.beginPath();
      context.moveTo(x, y);
      context.lineTo(Xfinal, Yfinal);
      context.arc(x, y, r, aPartida, aFinal);
      context.closePath();
      context.fill();
      context.stroke();
      console.log(thehta_inci2);
      console.log(aFinal);

      var incidente = parseFloat(tercero2.innerHTML, 10);
      var critico = parseFloat(deltax4.innerHTML, 10);
      console.log(incidente);
      console.log(critico);

      //DATOS TABLA DINAMINCA
      let nombre_materila2 = document.getElementById("coeficiente n4").value;
      material4.innerHTML = nombre_materila2;

      let valor_material2 = document.getElementById("coeficiente n4");
      let indice2 = valor_material2.options[valor_material2.selectedIndex].text;
      valor4.innerHTML = indice2;

      ángulo_incidente4.innerHTML = tercero2.innerHTML;
      ángulo_critico4.innerHTML = deltax4.innerHTML;
      ángulo_refracción4.innerHTML = cuarto2.innerHTML;
      distancia4.innerHTML = deltay4.innerHTML;

      //suma de las distancias
      let dis1 = parseFloat(deltay.innerHTML);
      let dis2 = parseFloat(deltay2.innerHTML);
      let dis3 = parseFloat(deltay3.innerHTML);
      let dis4 = parseFloat(deltay4.innerHTML);
      let dis5 = parseFloat(deltay5.innerHTML);
      let dis6 = parseFloat(deltay6.innerHTML);
      let dis7 = parseFloat(deltay7.innerHTML);
      let dis8 = parseFloat(deltay8.innerHTML);
      let dis9 = parseFloat(deltay9.innerHTML);
      let dis10 = parseFloat(deltay10.innerHTML);

      let multiplier = 100;
      distancia_total.innerHTML =
        Math.round(
          (dis1 +
            dis2 +
            dis3 +
            dis4 +
            dis5 +
            dis6 +
            dis7 +
            dis8 +
            dis9 +
            dis10) *
            multiplier
        ) / multiplier;
      distancia_acumuluda4.innerHTML =
        Math.round((dis1 + dis2 + dis3 + dis4) * multiplier) / multiplier;

      //FIN TABLA DENAMICA

      if (incidente > critico) {
        // grafica del ángulo critico
        ctx4.beginPath();
        ctx4.arc(x, y, r, 0, 2 * Math.PI);
        ctx4.lineWidth = 3;
        ctx4.strokeStyle = "pink";
        ctx4.stroke();

        let thehta_inci = tercero2.innerHTML;
        let thehta_inci_n = parseFloat(thehta_inci, 10);
        console.log(thehta_inci_n);
        if (elCanvas && elCanvas.getContext) {
          context.beginPath();
          if (context) {
            var aPartida = (Math.PI / 180) * 180;
            var aFinal = (Math.PI / 180) * (-90 - thehta_inci_n);

            //context.arc(x, y, r, aPartida, aFinal, false);

            var Xfinal = x + r * Math.cos(aPartida);
            var Yfinal = y + r * Math.sin(aPartida);

            ctx4.fillStyle = "#abcdef";
            ctx4.strokeStyle = "red";
            ctx4.lineWidth = 3;

            //Dibujo:
            context.beginPath();
            context.moveTo(x, y);
            context.lineTo(Xfinal, Yfinal);
            context.arc(x, y, r, aPartida, aFinal);
            context.closePath();
            context.fill();
            context.stroke();
          }
        }
      }
    }
  }
}

//*********************************************************// */
function strokeCircle7(x, y, r) {
  ctx4.beginPath();
  ctx4.arc(x, y, r, 0, 2 * Math.PI);
  ctx4.lineWidth = 3;
  ctx4.strokeStyle = "pink";
  ctx4.stroke();

  let thehta_refra2 = cuarto2.innerHTML;
  let thehta_refra2_n = parseFloat(thehta_refra2, 10);
  console.log(thehta_refra2_n);

  var elCanvas = document.getElementById("c4");
  if (elCanvas && elCanvas.getContext) {
    var context = elCanvas.getContext("2d");
    context.beginPath();
    if (context) {
      var aPartida = (Math.PI / 180) * 90;
      var aFinal = (Math.PI / 180) * (90 + thehta_refra2_n);

      //context.arc(x, y, r, aPartida, aFinal, false);

      var Xfinal = x + r * Math.cos(aPartida);
      var Yfinal = y + r * Math.sin(aPartida);

      ctx4.fillStyle = "#abcdef";
      ctx4.strokeStyle = "#1E90FF";
      ctx4.lineWidth = 3;

      //Dibujo:
      context.beginPath();
      context.moveTo(x, y);
      context.lineTo(Xfinal, Yfinal);
      context.arc(x, y, r, aPartida, aFinal);
      context.closePath();
      context.fill();
      context.stroke();
      console.log(thehta_refra2);
      console.log(aFinal);
    }
  }
}
