var output6 = document.getElementById("output6");
var sexto = document.getElementById("sexto");
var sexto2 = document.getElementById("sexto2");
var deltax6 = document.getElementById("deltax6");
var deltay6 = document.getElementById("deltay6");
var c6 = document.getElementById("c6");
var ctx6 = c6.getContext("2d");

var cw = (c6.width = 300),
  cx = cw / 2;
var ch = (c6.height = 300),
  cy = ch / 2;
//var rad = Math.PI / 180;
var R = 100,
  r = 5;

var handle6 = {
  x: cx + R,
  y: cy,
  r: 5,
};
output6.style.top = handle6.y - 30 + "px";
output6.style.left = handle6.x - 30 + "px";

var isDragging = false;
ctx6.strokeStyle = "#555";
ctx6.fillStyle = "#e18728";
// circle

drawAxes6();
strokeCircle10(cx, cy, R);
strokeCircle11(cx, cy, R);
//drawHandle2(handle2);
drawHub6();

// Events ***************************

c6.addEventListener(
  "mousedown",
  function (evt) {
    isDragging = true;
    updateHandle6(evt);
  },
  false
);

// mousemove
c6.addEventListener(
  "mousemove",
  function (evt) {
    if (isDragging) {
      updateHandle6(evt);
    }
  },
  false
);
// mouseup
c6.addEventListener(
  "mouseup",
  function () {
    isDragging = false;
  },
  false
);
// mouseout
c6.addEventListener(
  "mouseout",
  function (evt) {
    isDragging = false;
  },
  false
); /**/

// Helpers ***************************
function strokeCircle10(x, y, r) {
  ctx6.beginPath();
  ctx6.arc(x, y, r, 0, 2 * Math.PI);
  ctx6.lineWidth = 3;
  ctx6.strokeStyle = "black";
  ctx6.stroke();

  var elCanvas6 = document.getElementById("c6");
  if (elCanvas6 && elCanvas6.getContext) {
    var context6 = elCanvas6.getContext("2d");
    context6.beginPath();
    if (context6) {
      var aPartida6 = (Math.PI / 180) * 270;
      var aFinal6 = (Math.PI / 180) * 320;
      context6.strokeStyle = "blue";
      context6.lineWidth = 3;
      context6.arc(x, y, r, aPartida6, aFinal6, false);
      context6.stroke();
    }
  }
}

function strokeCircle11(x, y, r) {
  ctx6.beginPath();
  ctx6.arc(x, y, r, 0, 2 * Math.PI);
  ctx6.lineWidth = 3;
  ctx6.strokeStyle = "black";
  ctx6.stroke();

  var elCanvas6 = document.getElementById("c6");
  if (elCanvas6 && elCanvas6.getContext) {
    var context6 = elCanvas6.getContext("2d");
    context6.beginPath();
    if (context6) {
      var aPartida6 = (Math.PI / 180) * 270;
      var aFinal6 = (Math.PI / 180) * 320;
      context6.strokeStyle = "blue";
      context6.lineWidth = 3;
      context6.arc(x, y, r, aPartida6, aFinal6, false);
      context6.stroke();
    }
  }
}

function fillCircle6(x, y, r) {
  ctx6.beginPath();
  ctx6.arc(x, y, r, 0, 2 * Math.PI);
  ctx6.save();
  ctx6.strokeStyle = "#ccc";
  ctx6.lineWidth = 3;
  ctx6.fill();
  ctx6.stroke();
  ctx6.restore();
}

function drawHub6() {
  ctx6.save();
  ctx6.fillStyle = "black";
  fillCircle6(cx, cy, 3);
  ctx6.restore();
}

function drawAxes6() {
  ctx6.save();
  ctx6.setLineDash([4, 4]);
  ctx6.strokeStyle = "#ccc";
  ctx6.beginPath();
  ctx6.moveTo(cx, 0);
  ctx6.lineTo(cx, ch); //Recta eje vertical
  ctx6.moveTo(0, cy); // Recta eje horizontal
  ctx6.lineTo(cw, cy);
  ctx6.stroke();
  ctx6.restore();
}

function oMousePos6(canvas, evt) {
  var rect = canvas.getBoundingClientRect();
  return {
    x: Math.round(evt.clientX - rect.left),
    y: Math.round(evt.clientY - rect.top),
  };
}

function updateHandle6(evt) {
  var m = oMousePos5(c6, evt);
  var deltaX = m.x - cx;
  var deltaY = m.y - cy;
  handle6.a = Math.atan2(deltaY, deltaX);
  handle6.b = Math.atan(deltaY / deltaX);
  handle6.x = cx + R * Math.cos(handle6.a);
  handle6.y = cy + R * Math.sin(handle6.a);
  ctx6.clearRect(0, 0, cw, ch);

  drawAxes6();
  strokeCircle10(cx, cy, R);
  strokeCircle11(cx, cy, R);
  //drawHandle2(handle);
  drawHub6();

  output6.innerHTML = quinto2.innerHTML + "°";
  sexto.innerHTML = quinto2.innerHTML;

  var thehta_refracción_n2 = document.getElementById("quinto2").innerHTML;
  console.log(thehta_refracción_n2);

  /* Para obtener el valor */
  var nombre1 = document.getElementById("coeficiente n6").value;
  console.log(nombre1);
  var nombre2 = document.getElementById("coeficiente n7").value;
  console.log(nombre2);

  /* Para obtener el texto */
  var combo1 = document.getElementById("coeficiente n6");
  var selected1 = combo1.options[combo1.selectedIndex].text;
  var combo2 = document.getElementById("coeficiente n7");
  var selected2 = combo2.options[combo2.selectedIndex].text;

  console.log(selected1);
  console.log(selected2);
  n6 = parseFloat(selected1, 10);
  n7 = parseFloat(selected2, 10);

  let gra_rad = thehta_refracción_n2 * (Math.PI / 180);
  let n = (n6 / n7) * Math.sin(gra_rad);
  var ns = Math.asin(n);
  let thehta_refracción_large = ns * (180 / Math.PI);
  console.log(gra_rad);
  console.log(n);
  console.log(ns);
  console.log(thehta_refracción_large);

  //FIN de la distancia reccorida. Continuacion con el ángulo de refracción
  let multiplier = 100;
  let thehta_refracción =
    Math.round(thehta_refracción_large * multiplier) / multiplier;
  sexto2.innerHTML = thehta_refracción;

  var thehta_critico = Math.asin(n7 / n6);
  var thehta_critico_degree_large = thehta_critico * (180 / Math.PI);
  console.log(thehta_critico);
  console.log(thehta_critico_degree_large); //se imprime angulo critico valor completo
  let thehta_critico_degree =
    Math.round(thehta_critico_degree_large * multiplier) / multiplier; // se redondea el angulo critico
  deltax6.innerHTML = thehta_critico_degree;
  console.log(deltax6);

  //Distancia recorrida en cm

  /* Para obtener el texto */
  var combo = document.getElementById("e6");
  var selected = combo.options[combo.selectedIndex].text;

  console.log(selected);
  e6 = parseFloat(selected, 10);

  var cos_refracción_rad = Math.cos(Math.asin(n));
  var distancia_rad = e6 / cos_refracción_rad;
  console.log(distancia_rad);
  let distancia = Math.round(distancia_rad * multiplier) / multiplier;
  deltay6.innerHTML = distancia;
}

function strokeCircle10(x, y, r) {
  ctx6.beginPath();
  ctx6.arc(x, y, r, 0, 2 * Math.PI);
  ctx6.lineWidth = 3;
  ctx6.strokeStyle = "pink";
  ctx6.stroke();

  let thehta_inci2 = quinto2.innerHTML;
  let thehta_inci2_n = parseFloat(thehta_inci2, 10);
  console.log(thehta_inci2_n);

  var elCanvas = document.getElementById("c6");
  if (elCanvas && elCanvas.getContext) {
    var context = elCanvas.getContext("2d");
    context.beginPath();
    if (context) {
      var aPartida = (Math.PI / 180) * -90;
      var aFinal = (Math.PI / 180) * (-90 + thehta_inci2_n);

      //context.arc(x, y, r, aPartida, aFinal, false);

      var Xfinal = x + r * Math.cos(aPartida);
      var Yfinal = y + r * Math.sin(aPartida);

      ctx6.fillStyle = "#abcdef";
      ctx6.strokeStyle = "#1E90FF";
      ctx6.lineWidth = 3;

      //Dibujo:
      context.beginPath();
      context.moveTo(x, y);
      context.lineTo(Xfinal, Yfinal);
      context.arc(x, y, r, aPartida, aFinal);
      context.closePath();
      context.fill();
      context.stroke();
      console.log(thehta_inci2);
      console.log(aFinal);

      var incidente = parseFloat(quinto2.innerHTML, 10);
      var critico = parseFloat(deltax6.innerHTML, 10);
      console.log(incidente);
      console.log(critico);

      //DATOS TABLA DINAMINCA
      let nombre_materila2 = document.getElementById("coeficiente n6").value;
      material6.innerHTML = nombre_materila2;

      let valor_material2 = document.getElementById("coeficiente n6");
      let indice2 = valor_material2.options[valor_material2.selectedIndex].text;
      valor6.innerHTML = indice2;

      ángulo_incidente6.innerHTML = quinto2.innerHTML;
      ángulo_critico6.innerHTML = deltax6.innerHTML;
      ángulo_refracción6.innerHTML = sexto2.innerHTML;
      distancia6.innerHTML = deltay6.innerHTML;

      //suma de las distancias
      let dis1 = parseFloat(deltay.innerHTML);
      let dis2 = parseFloat(deltay2.innerHTML);
      let dis3 = parseFloat(deltay3.innerHTML);
      let dis4 = parseFloat(deltay4.innerHTML);
      let dis5 = parseFloat(deltay5.innerHTML);
      let dis6 = parseFloat(deltay6.innerHTML);
      let dis7 = parseFloat(deltay7.innerHTML);
      let dis8 = parseFloat(deltay8.innerHTML);
      let dis9 = parseFloat(deltay9.innerHTML);
      let dis10 = parseFloat(deltay10.innerHTML);

      let multiplier = 100;
      distancia_total.innerHTML =
        Math.round(
          (dis1 +
            dis2 +
            dis3 +
            dis4 +
            dis5 +
            dis6 +
            dis7 +
            dis8 +
            dis9 +
            dis10) *
            multiplier
        ) / multiplier;
      distancia_acumuluda6.innerHTML =
        Math.round((dis1 + dis2 + dis3 + dis4 + dis5 + dis6) * multiplier) /
        multiplier;

      //FIN TABLA DENAMICA

      if (incidente > critico) {
        // grafica del ángulo critico
        ctx6.beginPath();
        ctx6.arc(x, y, r, 0, 2 * Math.PI);
        ctx6.lineWidth = 3;
        ctx6.strokeStyle = "pink";
        ctx6.stroke();

        let thehta_inci = quinto2.innerHTML;
        let thehta_inci_n = parseFloat(thehta_inci, 10);
        console.log(thehta_inci_n);
        if (elCanvas && elCanvas.getContext) {
          context.beginPath();
          if (context) {
            var aPartida = (Math.PI / 180) * 180;
            var aFinal = (Math.PI / 180) * (-90 - thehta_inci_n);

            //context.arc(x, y, r, aPartida, aFinal, false);

            var Xfinal = x + r * Math.cos(aPartida);
            var Yfinal = y + r * Math.sin(aPartida);

            ctx6.fillStyle = "#abcdef";
            ctx6.strokeStyle = "red";
            ctx6.lineWidth = 3;

            //Dibujo:
            context.beginPath();
            context.moveTo(x, y);
            context.lineTo(Xfinal, Yfinal);
            context.arc(x, y, r, aPartida, aFinal);
            context.closePath();
            context.fill();
            context.stroke();
          }
        }
      }
    }
  }
}

//*********************************************************// */
function strokeCircle11(x, y, r) {
  ctx6.beginPath();
  ctx6.arc(x, y, r, 0, 2 * Math.PI);
  ctx6.lineWidth = 3;
  ctx6.strokeStyle = "pink";
  ctx6.stroke();

  let thehta_refra2 = sexto2.innerHTML;
  let thehta_refra2_n = parseFloat(thehta_refra2, 10);
  console.log(thehta_refra2_n);

  var elCanvas = document.getElementById("c6");
  if (elCanvas && elCanvas.getContext) {
    var context = elCanvas.getContext("2d");
    context.beginPath();
    if (context) {
      var aPartida = (Math.PI / 180) * 90;
      var aFinal = (Math.PI / 180) * (90 + thehta_refra2_n);

      //context.arc(x, y, r, aPartida, aFinal, false);

      var Xfinal = x + r * Math.cos(aPartida);
      var Yfinal = y + r * Math.sin(aPartida);

      ctx6.fillStyle = "#abcdef";
      ctx6.strokeStyle = "#1E90FF";
      ctx6.lineWidth = 3;

      //Dibujo:
      context.beginPath();
      context.moveTo(x, y);
      context.lineTo(Xfinal, Yfinal);
      context.arc(x, y, r, aPartida, aFinal);
      context.closePath();
      context.fill();
      context.stroke();
      console.log(thehta_refra2);
      console.log(aFinal);
    }
  }
}
