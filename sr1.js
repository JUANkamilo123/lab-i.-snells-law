var output = document.getElementById("output");
var Atan = document.getElementById("atan");
var Atan2 = document.getElementById("atan2");
var deltax = document.getElementById("deltax");
var deltay = document.getElementById("deltay");
var c = document.getElementById("c");
var ctx = c.getContext("2d");

var cw = (c.width = 300),
  cx = cw / 2;
var ch = (c.height = 300),
  cy = ch / 2;
//var rad = Math.PI / 180;
var R = 100,
  r = 5;

var handle = {
  x: cx + R,
  y: cy,
  r: 5,
};
output.style.top = handle.y - 30 + "px";
output.style.left = handle.x - 30 + "px";

var isDragging = false;
ctx.strokeStyle = "#555";
ctx.fillStyle = "#e18728";
// circle

drawAxes();
strokeCircle(cx, cy, R);
drawHandle(handle);
drawHub();

// Events ***************************

c.addEventListener(
  "mousedown",
  function (evt) {
    isDragging = true;
    updateHandle(evt);
  },
  false
);

// mousemove
c.addEventListener(
  "mousemove",
  function (evt) {
    if (isDragging) {
      updateHandle(evt);
    }
  },
  false
);
// mouseup
c.addEventListener(
  "mouseup",
  function () {
    isDragging = false;
  },
  false
);
// mouseout
c.addEventListener(
  "mouseout",
  function (evt) {
    isDragging = false;
  },
  false
); /**/

// Helpers ***************************
function strokeCircle(x, y, r) {
  ctx.beginPath();
  ctx.arc(x, y, r, 0, 2 * Math.PI);
  ctx.lineWidth = 3;
  ctx.strokeStyle = "black";
  ctx.stroke();

  var elCanvas = document.getElementById("c");
  if (elCanvas && elCanvas.getContext) {
    var context = elCanvas.getContext("2d");
    context.beginPath();
    if (context) {
      var aPartida = (Math.PI / 180) * 270;
      var aFinal = (Math.PI / 180) * 320;
      context.strokeStyle = "blue";
      context.lineWidth = 3;
      context.arc(x, y, r, aPartida, aFinal, false);
      context.stroke();
    }
  }
}

function fillCircle(x, y, r) {
  ctx.beginPath();
  ctx.arc(x, y, r, 0, 2 * Math.PI);
  ctx.save();
  ctx.strokeStyle = "#ccc";
  ctx.lineWidth = 3;
  ctx.fill();
  ctx.stroke();
  ctx.restore();
}

function drawHub() {
  ctx.save();
  ctx.fillStyle = "black";
  fillCircle(cx, cy, 3);
  ctx.restore();
}

function drawAxes() {
  ctx.save();
  ctx.setLineDash([4, 4]);
  ctx.strokeStyle = "#ccc";
  ctx.beginPath();
  ctx.moveTo(cx, 0);
  ctx.lineTo(cx, ch); //Recta eje vertical
  ctx.moveTo(0, cy); // Recta eje horizontal
  ctx.lineTo(cw, cy);
  ctx.stroke();
  ctx.restore();
}

function oMousePos(canvas, evt) {
  var rect = canvas.getBoundingClientRect();
  return {
    x: Math.round(evt.clientX - rect.left),
    y: Math.round(evt.clientY - rect.top),
  };
}

function drawHandle(handle, x, y, r) {
  // Linea de eje

  ctx.beginPath();
  ctx.moveTo(cx, cy);
  ctx.lineTo(handle.x, handle.y);
  ctx.stroke();

  fillCircle(handle.x, handle.y, handle.r);
}

// Obtención del coeficiente de Refracción
function ShowSelected(x, y, r) {}

function updateHandle(evt) {
  var m = oMousePos(c, evt);
  var deltaX = m.x - cx;
  var deltaY = m.y - cy;
  handle.a = Math.atan2(deltaY, deltaX);
  handle.b = Math.atan(deltaY / deltaX);
  handle.x = cx + R * Math.cos(handle.a);
  handle.y = cy + R * Math.sin(handle.a);
  ctx.clearRect(0, 0, cw, ch);

  drawAxes();
  strokeCircle(cx, cy, R);
  drawHandle(handle);
  drawHub();

  output.innerHTML = parseInt(90 + handle.b * (180 / Math.PI)) + "°";
  //output.style.top = handle.y - 30 + "px";
  //output.style.left = handle.x - 30 + "px";
  Atan.innerHTML = (90 + handle.b * (180 / Math.PI)).toFixed(2);
  //Atan2.innerHTML = (handle.a * (180 / Math.PI)).toFixed(2);
  //deltax.innerHTML = deltaX;
  //deltay.innerHTML = deltaY;

  var thetha_incidente = document.getElementById("atan").innerHTML;
  console.log(thetha_incidente);

  /* Para obtener el valor */
  var nombre1 = document.getElementById("coeficiente n1").value;
  console.log(nombre1);
  var nombre2 = document.getElementById("coeficiente n2").value;
  console.log(nombre2);

  /* Para obtener el texto */
  var combo1 = document.getElementById("coeficiente n1");
  var selected1 = combo1.options[combo1.selectedIndex].text;
  var combo2 = document.getElementById("coeficiente n2");
  var selected2 = combo2.options[combo2.selectedIndex].text;

  console.log(selected1);
  console.log(selected2);
  n1 = parseFloat(selected1, 10);
  n2 = parseFloat(selected2, 10);

  let gra_rad = thetha_incidente * (Math.PI / 180);
  let n = (n1 / n2) * Math.sin(gra_rad);
  var ns = Math.asin(n);
  let thehta_refracción_large = ns * (180 / Math.PI);
  console.log(gra_rad);
  console.log(n);
  console.log(ns);
  console.log(thehta_refracción_large);

  let multiplier = 100;
  let thehta_refracción =
    Math.round(thehta_refracción_large * multiplier) / multiplier;
  Atan2.innerHTML = thehta_refracción;

  var thehta_critico = Math.asin(n2 / n1);
  var thehta_critico_degree_large = thehta_critico * (180 / Math.PI);
  console.log(thehta_critico);
  console.log(thehta_critico_degree_large); //se imprime angulo critico valor completo
  let thehta_critico_degree =
    Math.round(thehta_critico_degree_large * multiplier) / multiplier; // se redondea el angulo critico
  deltax.innerHTML = thehta_critico_degree;

  //Distancia recorrida en cm

  /* Para obtener el texto */
  var combo = document.getElementById("e1");
  var selected = combo.options[combo.selectedIndex].text;

  console.log(selected);
  e1 = parseFloat(selected, 10);

  var cos_refracción_rad = Math.cos(gra_rad);
  var distancia_rad = e1 / cos_refracción_rad;
  console.log(distancia_rad);
  let distancia = Math.round(distancia_rad * multiplier) / multiplier;
  deltay.innerHTML = distancia;
}

function strokeCircle(x, y, r) {
  ctx.beginPath();
  ctx.arc(x, y, r, 0, 2 * Math.PI);
  ctx.lineWidth = 3;
  ctx.strokeStyle = "pink";
  ctx.stroke();

  let thehta_refra = Atan2.innerHTML;
  let thehta_refra_n = parseFloat(thehta_refra, 10);
  console.log(thehta_refra_n);
  var elCanvas = document.getElementById("c");
  if (elCanvas && elCanvas.getContext) {
    var context = elCanvas.getContext("2d");
    context.beginPath();
    if (context) {
      var aPartida = (Math.PI / 180) * 90;
      var aFinal = (Math.PI / 180) * (thehta_refra_n + 90);

      //context.arc(x, y, r, aPartida, aFinal, false);

      var Xfinal = x + r * Math.cos(aPartida);
      var Yfinal = y + r * Math.sin(aPartida);

      ctx.fillStyle = "#abcdef";
      ctx.strokeStyle = "#1E90FF";
      ctx.lineWidth = 3;

      //Dibujo:
      context.beginPath();
      context.moveTo(x, y);
      context.lineTo(Xfinal, Yfinal);
      context.arc(x, y, r, aPartida, aFinal);
      context.closePath();
      context.fill();
      context.stroke();
      console.log(thehta_refra);
      console.log(aFinal);

      var incidente = parseFloat(Atan.innerHTML, 10);
      var critico = parseFloat(deltax.innerHTML, 10);
      console.log(incidente);
      console.log(critico);

      //DATOS TABLA DINAMINCA
      let nombre_material = document.getElementById("coeficiente n1").value;
      material1.innerHTML = nombre_material;

      let valor_material1 = document.getElementById("coeficiente n1");
      let indice1 = valor_material1.options[valor_material1.selectedIndex].text;
      valor1.innerHTML = indice1;

      ángulo_incidente.innerHTML = atan.innerHTML;
      ángulo_critico.innerHTML = deltax.innerHTML;
      ángulo_refracción.innerHTML = atan2.innerHTML;
      distancia1.innerHTML = deltay.innerHTML;

      let dis1 = parseFloat(deltay.innerHTML);
      let dis2 = parseFloat(deltay2.innerHTML);
      let dis3 = parseFloat(deltay3.innerHTML);
      let dis4 = parseFloat(deltay4.innerHTML);
      let dis5 = parseFloat(deltay5.innerHTML);
      let dis6 = parseFloat(deltay6.innerHTML);
      let dis7 = parseFloat(deltay7.innerHTML);
      let dis8 = parseFloat(deltay8.innerHTML);
      let dis9 = parseFloat(deltay9.innerHTML);
      let dis10 = parseFloat(deltay10.innerHTML);

      let multiplier = 100;
      distancia_total.innerHTML =
        Math.round(
          (dis1 +
            dis2 +
            dis3 +
            dis4 +
            dis5 +
            dis6 +
            dis7 +
            dis8 +
            dis9 +
            dis10) *
            multiplier
        ) / multiplier;
      distancia_acumuluda1.innerHTML =
        Math.round(dis1 * multiplier) / multiplier;

      //FIN TABLA DENAMICA

      if (incidente > critico) {
        // grafica del ángulo critico
        ctx.beginPath();
        ctx.arc(x, y, r, 0, 2 * Math.PI);
        ctx.lineWidth = 3;
        ctx.strokeStyle = "pink";
        ctx.stroke();

        let thehta_inci = Atan.innerHTML;
        let thehta_inci_n = parseFloat(thehta_inci, 10);
        console.log(thehta_inci_n);
        if (elCanvas && elCanvas.getContext) {
          context.beginPath();
          if (context) {
            var aPartida = (Math.PI / 180) * 180;
            var aFinal = (Math.PI / 180) * (-90 - thehta_inci_n);

            //context.arc(x, y, r, aPartida, aFinal, false);

            var Xfinal = x + r * Math.cos(aPartida);
            var Yfinal = y + r * Math.sin(aPartida);

            ctx.fillStyle = "#abcdef";
            ctx.strokeStyle = "red";
            ctx.lineWidth = 3;

            //Dibujo:
            context.beginPath();
            context.moveTo(x, y);
            context.lineTo(Xfinal, Yfinal);
            context.arc(x, y, r, aPartida, aFinal);
            context.closePath();
            context.fill();
            context.stroke();
          }
        }
      }
    }
  }
}

//*********************************************************// */
