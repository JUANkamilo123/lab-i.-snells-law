var output2 = document.getElementById("output3");
var tercero = document.getElementById("tercero");
var tercero2 = document.getElementById("tercero2");
var deltax3 = document.getElementById("deltax3");
var deltay3 = document.getElementById("deltay3");
var c3 = document.getElementById("c3");
var ctx3 = c3.getContext("2d");

var cw = (c3.width = 300),
  cx = cw / 2;
var ch = (c3.height = 300),
  cy = ch / 2;
//var rad = Math.PI / 180;
var R = 100,
  r = 5;

var handle3 = {
  x: cx + R,
  y: cy,
  r: 5,
};
output3.style.top = handle3.y - 30 + "px";
output3.style.left = handle3.x - 30 + "px";

var isDragging = false;
ctx3.strokeStyle = "#555";
ctx3.fillStyle = "#e18728";
// circle

drawAxes3();
strokeCircle4(cx, cy, R);
strokeCircle5(cx, cy, R);
//drawHandle2(handle2);
drawHub3();

// Events ***************************

c3.addEventListener(
  "mousedown",
  function (evt) {
    isDragging = true;
    updateHandle3(evt);
  },
  false
);

// mousemove
c3.addEventListener(
  "mousemove",
  function (evt) {
    if (isDragging) {
      updateHandle3(evt);
    }
  },
  false
);
// mouseup
c3.addEventListener(
  "mouseup",
  function () {
    isDragging = false;
  },
  false
);
// mouseout
c3.addEventListener(
  "mouseout",
  function (evt) {
    isDragging = false;
  },
  false
); /**/

// Helpers ***************************
function strokeCircle4(x, y, r) {
  ctx3.beginPath();
  ctx3.arc(x, y, r, 0, 2 * Math.PI);
  ctx3.lineWidth = 3;
  ctx3.strokeStyle = "black";
  ctx3.stroke();

  var elCanvas3 = document.getElementById("c3");
  if (elCanvas3 && elCanvas3.getContext) {
    var context3 = elCanvas3.getContext("2d");
    context3.beginPath();
    if (context3) {
      var aPartida3 = (Math.PI / 180) * 270;
      var aFinal3 = (Math.PI / 180) * 320;
      context3.strokeStyle = "blue";
      context3.lineWidth = 3;
      context3.arc(x, y, r, aPartida3, aFinal3, false);
      context3.stroke();
    }
  }
}

function strokeCircle5(x, y, r) {
  ctx3.beginPath();
  ctx3.arc(x, y, r, 0, 2 * Math.PI);
  ctx3.lineWidth = 3;
  ctx3.strokeStyle = "black";
  ctx3.stroke();

  var elCanvas3 = document.getElementById("c3");
  if (elCanvas3 && elCanvas3.getContext) {
    var context3 = elCanvas3.getContext("2d");
    context3.beginPath();
    if (context3) {
      var aPartida3 = (Math.PI / 180) * 270;
      var aFinal3 = (Math.PI / 180) * 320;
      context3.strokeStyle = "blue";
      context3.lineWidth = 3;
      context3.arc(x, y, r, aPartida3, aFinal3, false);
      context3.stroke();
    }
  }
}

function fillCircle3(x, y, r) {
  ctx3.beginPath();
  ctx3.arc(x, y, r, 0, 2 * Math.PI);
  ctx3.save();
  ctx3.strokeStyle = "#ccc";
  ctx3.lineWidth = 3;
  ctx3.fill();
  ctx3.stroke();
  ctx3.restore();
}

function drawHub3() {
  ctx3.save();
  ctx3.fillStyle = "black";
  fillCircle3(cx, cy, 3);
  ctx3.restore();
}

function drawAxes3() {
  ctx3.save();
  ctx3.setLineDash([4, 4]);
  ctx3.strokeStyle = "#ccc";
  ctx3.beginPath();
  ctx3.moveTo(cx, 0);
  ctx3.lineTo(cx, ch); //Recta eje vertical
  ctx3.moveTo(0, cy); // Recta eje horizontal
  ctx3.lineTo(cw, cy);
  ctx3.stroke();
  ctx3.restore();
}

function oMousePos3(canvas, evt) {
  var rect = canvas.getBoundingClientRect();
  return {
    x: Math.round(evt.clientX - rect.left),
    y: Math.round(evt.clientY - rect.top),
  };
}

// function drawHandle2(handle2, x, y, r) {
//   // Linea de eje

//   ctx2.beginPath();
//   ctx2.moveTo(cx, cy);
//   ctx2.lineTo(handle2.x, handle2.y);
//   ctx2.stroke();

//   fillCircle2(handle2.x, handle2.y, handle2.r);
// }

// Obtención del coeficiente de Refracción
function ShowSelected(x, y, r) {}

function updateHandle3(evt) {
  var m = oMousePos2(c3, evt);
  var deltaX = m.x - cx;
  var deltaY = m.y - cy;
  handle3.a = Math.atan2(deltaY, deltaX);
  handle3.b = Math.atan(deltaY / deltaX);
  handle3.x = cx + R * Math.cos(handle3.a);
  handle3.y = cy + R * Math.sin(handle3.a);
  ctx3.clearRect(0, 0, cw, ch);

  drawAxes3();
  strokeCircle4(cx, cy, R);
  strokeCircle5(cx, cy, R);
  //drawHandle2(handle);
  drawHub3();

  output3.innerHTML = datos2.innerHTML + "°";
  tercero.innerHTML = datos2.innerHTML;

  var thehta_refracción_n2 = document.getElementById("datos2").innerHTML;
  console.log(thehta_refracción_n2);

  /* Para obtener el valor */
  var nombre1 = document.getElementById("coeficiente n3").value;
  console.log(nombre1);
  var nombre2 = document.getElementById("coeficiente n4").value;
  console.log(nombre2);

  /* Para obtener el texto */
  var combo1 = document.getElementById("coeficiente n3");
  var selected1 = combo1.options[combo1.selectedIndex].text;
  var combo2 = document.getElementById("coeficiente n4");
  var selected2 = combo2.options[combo2.selectedIndex].text;

  console.log(selected1);
  console.log(selected2);
  n3 = parseFloat(selected1, 10);
  n4 = parseFloat(selected2, 10);

  let gra_rad = thehta_refracción_n2 * (Math.PI / 180);
  let n = (n3 / n4) * Math.sin(gra_rad);
  var ns = Math.asin(n);
  let thehta_refracción_large = ns * (180 / Math.PI);
  console.log(gra_rad);
  console.log(n);
  console.log(ns);
  console.log(thehta_refracción_large);

  //FIN de la distancia reccorida. Continuacion con el ángulo de refracción
  let multiplier = 100;
  let thehta_refracción =
    Math.round(thehta_refracción_large * multiplier) / multiplier;
  tercero2.innerHTML = thehta_refracción;

  var thehta_critico = Math.asin(n4 / n3);
  var thehta_critico_degree_large = thehta_critico * (180 / Math.PI);
  console.log(thehta_critico);
  console.log(thehta_critico_degree_large); //se imprime angulo critico valor completo
  let thehta_critico_degree =
    Math.round(thehta_critico_degree_large * multiplier) / multiplier; // se redondea el angulo critico
  deltax3.innerHTML = thehta_critico_degree;
  console.log(deltax3);

  //Distancia recorrida en cm

  /* Para obtener el texto */
  var combo = document.getElementById("e3");
  var selected = combo.options[combo.selectedIndex].text;

  console.log(selected);
  e3 = parseFloat(selected, 10);

  var cos_refracción_rad = Math.cos(Math.asin(n));
  var distancia_rad = e3 / cos_refracción_rad;
  console.log(distancia_rad);
  let distancia = Math.round(distancia_rad * multiplier) / multiplier;
  deltay3.innerHTML = distancia;
}

function strokeCircle4(x, y, r) {
  ctx3.beginPath();
  ctx3.arc(x, y, r, 0, 2 * Math.PI);
  ctx3.lineWidth = 3;
  ctx3.strokeStyle = "pink";
  ctx3.stroke();

  let thehta_inci2 = datos2.innerHTML;
  let thehta_inci2_n = parseFloat(thehta_inci2, 10);
  console.log(thehta_inci2_n);

  var elCanvas = document.getElementById("c3");
  if (elCanvas && elCanvas.getContext) {
    var context = elCanvas.getContext("2d");
    context.beginPath();
    if (context) {
      var aPartida = (Math.PI / 180) * -90;
      var aFinal = (Math.PI / 180) * (-90 + thehta_inci2_n);

      //context.arc(x, y, r, aPartida, aFinal, false);

      var Xfinal = x + r * Math.cos(aPartida);
      var Yfinal = y + r * Math.sin(aPartida);

      ctx3.fillStyle = "#abcdef";
      ctx3.strokeStyle = "#1E90FF";
      ctx3.lineWidth = 3;

      //Dibujo:
      context.beginPath();
      context.moveTo(x, y);
      context.lineTo(Xfinal, Yfinal);
      context.arc(x, y, r, aPartida, aFinal);
      context.closePath();
      context.fill();
      context.stroke();
      console.log(thehta_inci2);
      console.log(aFinal);

      var incidente = parseFloat(datos2.innerHTML, 10);
      var critico = parseFloat(deltax3.innerHTML, 10);
      console.log(incidente);
      console.log(critico);

      //DATOS TABLA DINAMINCA
      let nombre_materila2 = document.getElementById("coeficiente n3").value;
      material3.innerHTML = nombre_materila2;

      let valor_material2 = document.getElementById("coeficiente n3");
      let indice2 = valor_material2.options[valor_material2.selectedIndex].text;
      valor3.innerHTML = indice2;

      ángulo_incidente3.innerHTML = datos2.innerHTML;
      ángulo_critico3.innerHTML = deltax3.innerHTML;
      ángulo_refracción3.innerHTML = tercero2.innerHTML;
      distancia3.innerHTML = deltay3.innerHTML;

      let dis1 = parseFloat(deltay.innerHTML);
      let dis2 = parseFloat(deltay2.innerHTML);
      let dis3 = parseFloat(deltay3.innerHTML);
      let dis4 = parseFloat(deltay4.innerHTML);
      let dis5 = parseFloat(deltay5.innerHTML);
      let dis6 = parseFloat(deltay6.innerHTML);
      let dis7 = parseFloat(deltay7.innerHTML);
      let dis8 = parseFloat(deltay8.innerHTML);
      let dis9 = parseFloat(deltay9.innerHTML);
      let dis10 = parseFloat(deltay10.innerHTML);

      let multiplier = 100;
      distancia_total.innerHTML =
        Math.round(
          (dis1 +
            dis2 +
            dis3 +
            dis4 +
            dis5 +
            dis6 +
            dis7 +
            dis8 +
            dis9 +
            dis10) *
            multiplier
        ) / multiplier;
      distancia_acumuluda3.innerHTML =
        Math.round((dis1 + dis2 + dis3) * multiplier) / multiplier;

      //FIN TABLA DENAMICA

      if (incidente > critico) {
        // grafica del ángulo critico
        ctx3.beginPath();
        ctx3.arc(x, y, r, 0, 2 * Math.PI);
        ctx3.lineWidth = 3;
        ctx3.strokeStyle = "pink";
        ctx3.stroke();

        let thehta_inci = datos2.innerHTML;
        let thehta_inci_n = parseFloat(thehta_inci, 10);
        console.log(thehta_inci_n);
        if (elCanvas && elCanvas.getContext) {
          context.beginPath();
          if (context) {
            var aPartida = (Math.PI / 180) * 180;
            var aFinal = (Math.PI / 180) * (-90 - thehta_inci_n);

            //context.arc(x, y, r, aPartida, aFinal, false);

            var Xfinal = x + r * Math.cos(aPartida);
            var Yfinal = y + r * Math.sin(aPartida);

            ctx3.fillStyle = "#abcdef";
            ctx3.strokeStyle = "red";
            ctx3.lineWidth = 3;

            //Dibujo:
            context.beginPath();
            context.moveTo(x, y);
            context.lineTo(Xfinal, Yfinal);
            context.arc(x, y, r, aPartida, aFinal);
            context.closePath();
            context.fill();
            context.stroke();
          }
        }
      }
    }
  }
}

//*********************************************************// */
function strokeCircle5(x, y, r) {
  ctx3.beginPath();
  ctx3.arc(x, y, r, 0, 2 * Math.PI);
  ctx3.lineWidth = 3;
  ctx3.strokeStyle = "pink";
  ctx3.stroke();

  let thehta_refra2 = tercero2.innerHTML;
  let thehta_refra2_n = parseFloat(thehta_refra2, 10);
  console.log(thehta_refra2_n);

  var elCanvas = document.getElementById("c3");
  if (elCanvas && elCanvas.getContext) {
    var context = elCanvas.getContext("2d");
    context.beginPath();
    if (context) {
      var aPartida = (Math.PI / 180) * 90;
      var aFinal = (Math.PI / 180) * (90 + thehta_refra2_n);

      //context.arc(x, y, r, aPartida, aFinal, false);

      var Xfinal = x + r * Math.cos(aPartida);
      var Yfinal = y + r * Math.sin(aPartida);

      ctx3.fillStyle = "#abcdef";
      ctx3.strokeStyle = "#1E90FF";
      ctx3.lineWidth = 3;

      //Dibujo:
      context.beginPath();
      context.moveTo(x, y);
      context.lineTo(Xfinal, Yfinal);
      context.arc(x, y, r, aPartida, aFinal);
      context.closePath();
      context.fill();
      context.stroke();
      console.log(thehta_refra2);
      console.log(aFinal);
    }
  }
}
